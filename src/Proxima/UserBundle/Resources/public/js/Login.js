var Login = function(parameters) {
    var $__s = this,
            $__params = parameters;
    var obj = {
        page: {
            /**
             * initializer
             * @returns {undefined}
             */
            _init: function() {
                $('body').addClass('login');
            }
        }
    };
    return {
        /**
         * class global register
         * @param {type} o
         * @returns {Main.Anonym$3}
         */
        register: function(o) {
            $__s.register(o);
            return this;
        },
        /**
         * intializer
         * @returns {undefined}
         */
        init: function() {
            obj.page._init();
        }
    };
};
Login.prototype = new EventDispatcher({}, 'login');