var User = function(parameters) {
    var $__s = this,
            $__params = parameters;
    var obj = {
        page: {
            /**
             * initializer
             * @returns {undefined}
             */
            _init: function() {

            }
        },
        form: {
            /**
             * initializer
             * @returns {undifined}
             */
            _bind: function() {
                var s = '.username-check';
                var timeout;
                $(s).keyup(function(e) {
                    if ($.inArray(e.which, [9, 16]) == -1) {
                        if ($(s).val().length > 0) {
                            clearTimeout(timeout);
                            timeout = setTimeout(function() {
                                obj.form._chechUserName();
                            }, 600);
                        }
                    }
                });
                $(document).on('click', '.suggestion', function(e) {
                    $('.username-check').val($(this).text()).trigger('keyup');
                    e.preventDefault();
                });
            },
            /**
             * check username availability
             * @returns {undefined}
             */
            _chechUserName: function() {
                var $this = $(".username-check");
                $.ajax({
                    url: Routing.generate('proxima_user_check_availability'),
                    data: {
                        username: $this.val()
                    },
                    type: "post",
                    success: function(data) {
                        if(data.isCheckable) {
                            if (data.available) {
                                $this.parent().next().html("<i class='icon-ok'></i> Username is available!");
                                $this.parents(".control-group").removeClass("error").addClass("success");
                            } else {
                                $this.parent().next().html("<i class='icon-remove'></i> Username not available!");
                                $this.parents(".control-group").removeClass("success").addClass("error");
                            }
                        } else {
                            $this.parent().next().html("<i class='icon-remove'></i> Username must have at least 5 characters!");
                            $this.parents(".control-group").removeClass("success").addClass("error");
                        }
                    },
                    beforeSend: function() {
                        $this.parent().next().html("<i class='icon-spinner icon-spin'></i> Checking availability...");
                    },
                    error: function() {
//                        $.unblockpage();
                        bootbox.hideAll();
                        $.alert({
                            msg: 'Server error please try again later.',
                            callback: function() {
                                $this.parent().next().html("Re-enter username!");
                                $this.parents(".control-group").removeClass("success").addClass("error");
                                bootbox.hideAll();
                            }
                        });
                    }
                });
            }
        }
    };
    return {
        /**
         * class global register
         * @param {type} o
         * @returns {Main.Anonym$3}
         */
        register: function(o) {
            $__s.register(o);
            return this;
        },
        /**
         * intializer
         * @returns {undefined}
         */
        init: function() {
            obj.page._init();
            obj.form._bind();
        }
    };
};
User.prototype = new EventDispatcher({}, 'user');