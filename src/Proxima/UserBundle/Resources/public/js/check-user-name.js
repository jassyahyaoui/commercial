$(document).ready(function() {
	if($(".username-check").length > 0){
		//ajax mocks
		$.mockjaxSettings.responseTime = 500; 
                $.mockjax({
			url: Routing.generate('proxima_user_check_availability', {username: $(".username-check").val()}),
			contentType: "text/json",
			response: function(settings) {
				this.responseText = {available: false, isCheckable: true};
				if(settings.data.username == ""){
                                    this.responseText = {available: false, isCheckable: false};
				}
			}
		});
	}
});
$(window).resize(function(){
	// console.log($(window).width());
});