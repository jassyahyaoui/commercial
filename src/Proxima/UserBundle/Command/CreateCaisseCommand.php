<?php

namespace Proxima\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCaisseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('caisse:create')
            ->setDescription('Create caisse')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manipulator = $this->getContainer()->get('proxima_user.load_manipulator');
        $manipulator->load('caisse', 0);

        $output->writeln(sprintf('caisse created !'));
    }
}