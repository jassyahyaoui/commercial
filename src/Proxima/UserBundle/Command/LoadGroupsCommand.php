<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Proxima\UserBundle\Command;

use Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\Console\Input\InputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * @author Wajih OUERIEMI <w.ourimi@gmail.com>
 */
class LoadGroupsCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('proxima:user:load-groups')
            ->setDescription('Generate static groups entity in database')
            ->setHelp(<<<EOT
The <info>proxima:user:load-groups</info> command generate the groups roles in database

  <info>php app/console proxima:user:load-groups</info>
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manipulator = $this->getContainer()->get('proxima_user.group_manipulator');
        $manipulator->load();
        $output->writeln(sprintf('Groups generated with roles ROLE_SUPER_USER, ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_USER'));
    }
}
