<?php

namespace Proxima\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Proxima\UserBundle\Entity\GroupRepository;

class UserType extends AbstractType
{
    /**
     *
     * @var Proxima\UserBundle\Entity\User $_current_user
     */    
    protected $_current_user;
    
    /**
     * @param Proxima\UserBundle\Entity\User $current_user The current user
     */
    public function __construct($current_user)
    {
        $this->_current_user = $current_user;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $this->buildUserForm($builder, $options);
        
        if( $this->_current_user->hasRole('Super User') || $this->_current_user->hasRole('Super Admin') || $this->_current_user->hasRole('Admin') ){
            $builder
                ->add('groups', 'entity' , array(
                    'label' => 'form.group',
                    'translation_domain' => 'ProximaUserBundle',
                    'class'       => 'ProximaUserBundle:Group',
                    'query_builder' => function(GroupRepository $gr){
                                            return $gr->getGroups($this->_current_user);
                                        },
                    'attr'  => array(
                        'class' => 'username-check',
                        'data-rule-required'=> 'true'
                    ),
                    'required'=>false,
                    'property' => 'name',
                ));
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Proxima\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'proxima_userbundle_usertype';
    }
    
    /**
     * 
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildUserForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null , array(
                'label' => 'form.username',
                'translation_domain' => 'ProximaUserBundle',
                'attr'  => array(
                    'class' => 'username-check',
                    'data-rule-required'=> 'true'
                ),
                'required'=>false,
            ))
            ->add('firstname', null , array(
                'label' => 'form.firstname',
                'translation_domain' => 'ProximaUserBundle',
                'attr'  => array(
                    'class' => 'input-xlarg',
                    'data-rule-required'=> 'true'
                ),
                'required'=>false,
            ))
            ->add('lastname', null , array(
                'label' => 'form.lastname',
                'translation_domain' => 'ProximaUserBundle',
                'attr'  => array(
                    'class' => 'input-xlarg',
                    'data-rule-required'=> 'true'
                ),
                'required'=>false,
            ))
            ->add('email', 'email' , array(
                'label' => 'form.email',
                'translation_domain' => 'ProximaUserBundle',
                'attr'  => array(
                    'class' => 'input-xlarg',
                    'data-rule-required'=> 'true'
                ),
                'required'=>false,
            ))
            ->add('enabled', 'checkbox', array(
                'label' => 'form.enabled',
                'translation_domain' => 'ProximaUserBundle',
                'attr'  => array(
                    'data-rule-required'=> 'false'
                ),
                'required'=>false,
            ))  
            ->add('plainPassword', 'repeated', array(
                'required'=>false,
                'translation_domain' => 'ProximaUserBundle',
                'type' => 'password',
                'first_options' => array(
                    'label' => 'form.new_password',
                    'attr'  => array(
                        'class' => 'complexify-me input-block-level',
                        'data-rule-required'=> 'true'
                        )
                    ),
                'second_options' => array(
                    'label' => 'form.new_password_confirmation',
                    'attr'  => array(
                        'class' => 'input-xlarg',
                        'data-rule-required'=> 'true',
                        'data-rule-equalto' => '#proxima_userbundle_usertype_plainPassword_first'
                        )
                    ),
                'invalid_message' => 'fos_user.password.mismatch',
            ));
    }
}