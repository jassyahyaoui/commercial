<?php

namespace Proxima\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProximaUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
