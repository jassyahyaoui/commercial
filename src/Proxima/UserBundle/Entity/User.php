<?php

namespace Proxima\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @var string
     */
    protected $firstname;

    /**
     * @var string
     */
    protected $lastname;
    
    /**
     * @var fullname
     */
    protected $fullname;
    
    /**
     * @var \Proxima\UserBundle\Entity\User
     */
    protected $created_by;

    /**
     * @var \Proxima\UserBundle\Entity\User
     */
    protected $updated_by;
    
    /**
     * @var \Commercial\CoreBundle\Entity\Account
     */
    protected $account;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    
    /**
     * Set fullname
     *
     * @return string 
     */
    public function setFullname()
    {
        $this->fullname = strtoupper(substr($this->firstname, 0, 1)).strtolower(substr_replace($this->firstname, '', 0, 1)).' '.strtoupper($this->lastname);
    }
    
    /**
     * Get lastname
     *
     * @return string 
     */
    public function getFullname()
    {
        return $this->fullname;
    }
    
    /**
     * Returns the user roles
     *
     * @return array The roles
     */
    public function getRoles()
    {
        $roles = $this->roles;
        
        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }

        return array_unique($roles);
    }
    
    /**
     * 
     * @param Group $group
     * @return User
     */
    public function addRole($group)
    {
        $this->addGroup($group);
        
        return $this;
    }
    /**
     * 
     * @param string $group
     * @return Boolean
     */
    public function hasRole($group)
    {
        return in_array($group, $this->getGroupNames());
    }
    
    /**
     * 
     * @param type $group
     * @return void
     */
    public function setGroups($group){
        $this->addGroup($group);
    }

    /**
     * Set created_by
     *
     * @param \Proxima\UserBundle\Entity\User $createdBy
     * @return User
     */
    public function setCreatedBy(\Proxima\UserBundle\Entity\User $createdBy = null)
    {
        $this->created_by = $createdBy;
    
        return $this;
    }

    /**
     * Get created_by
     *
     * @return \Proxima\UserBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set updated_by
     *
     * @param \Proxima\UserBundle\Entity\User $updatedBy
     * @return User
     */
    public function setUpdatedBy(\Proxima\UserBundle\Entity\User $updatedBy = null)
    {
        $this->updated_by = $updatedBy;
    
        return $this;
    }

    /**
     * Get updated_by
     *
     * @return \Proxima\UserBundle\Entity\User 
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * Set account
     *
     * @param \Commercial\CoreBundle\Entity\Account $account
     * @return User
     */
    public function setAccount(\Commercial\CoreBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Commercial\CoreBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
