<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Proxima\UserBundle\Util;

use \Proxima\UserBundle\Manager\ProximaLoadManager;

/**
 * Executes some manipulations on the users
 *
 * @author Christophe Coevoet <stof@notk.org>
 * @author Luis Cordova <cordoval@gmail.com>
 * @author Ahmed HACHENA <a.hachena@gmail.com>
 */
class ProximaLoadManipulator
{
    /**
     * User manager
     *
     * @var ProximaLoadManager
     */
    private $loadManager;

    public function __construct(ProximaLoadManager $loadManager)
    {
        $this->loadManager = $loadManager;
    }

    /**
     * Creates a group and returns it.
     *
     * @param string  $name
     * @param float  $solde
     *
     * @return void
     */
    public function load($name, $solde)
    {
        /** @var $caisse \Commercial\CoreBundle\Entity\Caisse */
        $caisse = $this->loadManager->createCaisse($name, $solde);
    }

}
