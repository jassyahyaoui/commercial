<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Proxima\UserBundle\Util;

use \Proxima\UserBundle\Manager\ProximaGroupManager;


const ROLE_SUPER_USER = 'ROLE_SUPER_USER';
const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
const ROLE_ADMIN = 'ROLE_ADMIN';
const ROLE_USER = 'ROLE_USER';

/**
 * Executes some manipulations on the users
 *
 * @author Christophe Coevoet <stof@notk.org>
 * @author Luis Cordova <cordoval@gmail.com>
 * @Author Wajih OUERIEMI <w.ourirmi@gmail.com>
 */
class ProximaGroupManipulator
{
    /**
     * User manager
     *
     * @var ProximaUserManager
     */
    private $groupManager;

    public function __construct(ProximaGroupManager $groupManager)
    {
        $this->groupManager = $groupManager;
    }

    /**
     * Creates a group and returns it.
     *
     * @param string  $name
     * @param array  $role
     *
     * @return void
     */
    public function load()
    {
        /** @var $group1 \Proxima\UserBundle\Entity\Group */
        $group1 = $this->groupManager->createGroup('Super User');
        $group1->addRole(ROLE_SUPER_USER);
        $this->groupManager->updateGroup($group1);
        
        /** @var $group2 \Proxima\UserBundle\Entity\Group */
        $group2 = $this->groupManager->createGroup('Super Admin');
        $group2->addRole(ROLE_SUPER_ADMIN);
        $this->groupManager->updateGroup($group2);
        
        /** @var $group3 \Proxima\UserBundle\Entity\Group */
        $group3 = $this->groupManager->createGroup('Admin');
        $group3->addRole(ROLE_ADMIN);
        $this->groupManager->updateGroup($group3);
        
        /** @var $group4 \Proxima\UserBundle\Entity\Group */
        $group4 = $this->groupManager->createGroup('User');
        $group4->addRole(ROLE_USER);
        $this->groupManager->updateGroup($group4);

    }

}
