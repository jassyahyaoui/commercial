<?php

/*
 * This file is part of the ProximaUserBundle package.
 * And implements FOSUserBundle package
 */

namespace Proxima\UserBundle\Manager;

use FOS\UserBundle\Util\CanonicalizerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Doctrine\ORM\EntityManager;

/**
 * Abstract User Manager implementation which can be used as base class for your
 * concrete manager.
 *
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Wajih OUERIEMI <w.ourimi@gmail.com> override
 */
class ProximaUserManager implements UserManagerInterface
{
    /**
     * @var EntityManager 
     */
    protected $entityManager;
    
    /**
     * @var EncoderFactoryInterface 
     */
    protected $encoderFactory;
    
    /**
     * @var CanonicalizerInterface 
     */
    protected $usernameCanonicalizer;
    
    /**
     * @var CanonicalizerInterface
     */
    protected $emailCanonicalizer;

    /**
     * Constructor.
     *
     * @param EntityManager $entityManager
     * @param EncoderFactoryInterface $encoderFactory
     * @param CanonicalizerInterface  $usernameCanonicalizer
     * @param CanonicalizerInterface  $emailCanonicalizer
     */
    public function __construct(EntityManager $entityManager, EncoderFactoryInterface $encoderFactory, CanonicalizerInterface $usernameCanonicalizer, CanonicalizerInterface $emailCanonicalizer)
    {
        $this->entityManager = $entityManager;
        $this->encoderFactory = $encoderFactory;
        $this->usernameCanonicalizer = $usernameCanonicalizer;
        $this->emailCanonicalizer = $emailCanonicalizer;
    }

    /**
     * Returns an empty user instance
     *
     * @return UserInterface
     */
    public function createUser()
    {
        $class = $this->getClass();
        /* @var $user \Proxima\UserBundle\Entity\User */
        $user = new $class;
        // we need to make sure to have at least one role
        $default_group = $this->entityManager->getRepository('ProximaUserBundle:Group')->findOneBy(array('name'=>'User'));
        $user->addGroup($default_group);
        
        return $user;
    }

    /**
     * Finds a user by email
     *
     * @param string $email
     *
     * @return UserInterface
     */
    public function findUserByEmail($email)
    {
        return $this->findUserBy(array('emailCanonical' => $this->canonicalizeEmail($email)));
    }

    /**
     * Finds a user by username
     *
     * @param string $username
     *
     * @return UserInterface
     */
    public function findUserByUsername($username)
    {
        return $this->findUserBy(array('usernameCanonical' => $this->canonicalizeUsername($username)));
    }

    /**
     * Finds a user either by email, or username
     *
     * @param string $usernameOrEmail
     *
     * @return UserInterface
     */
    public function findUserByUsernameOrEmail($usernameOrEmail)
    {
        if (filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL)) {
            return $this->findUserByEmail($usernameOrEmail);
        }

        return $this->findUserByUsername($usernameOrEmail);
    }

    /**
     * Finds a user either by confirmation token
     *
     * @param string $token
     *
     * @return UserInterface
     */
    public function findUserByConfirmationToken($token)
    {
        return $this->findUserBy(array('confirmationToken' => $token));
    }

    /**
     * Canonicalizes an email
     *
     * @param string $email
     *
     * @return string
     */
    protected function canonicalizeEmail($email)
    {
        return $this->emailCanonicalizer->canonicalize($email);
    }

    /**
     * Canonicalizes a username
     *
     * @param string $username
     *
     * @return string
     */
    protected function canonicalizeUsername($username)
    {
        return $this->usernameCanonicalizer->canonicalize($username);
    }

    protected function getEncoder(UserInterface $user)
    {
        return $this->encoderFactory->getEncoder($user);
    }

    public function deleteUser(UserInterface $user)
    {
        $this->entityManager->remove($user);
    }

    public function findUserBy(array $criteria = null)
    {
        if($criteria){
            $criteria_index_tab = array_keys($criteria);
            $criteria_index     = $criteria_index_tab[0];
        }
        $qb = $this->entityManager->createQueryBuilder()
              ->select('u, a, g')
              ->from($this->getClass(), 'u')
              ->leftJoin('u.groups', 'g')
              ->leftJoin('u.account', 'a');
              if($criteria){
                $qb->where(sprintf('u.%s = :criteria',$criteria_index))
                   ->setParameter('criteria', $criteria[$criteria_index]);
                
                return $qb->getQuery()->getOneOrNullResult();
              }
        return $qb->getQuery()->getResult();
    }

    public function findUsers()
    {
        return $this->findUserBy(null);
    }

    public function getClass()
    {
        return 'Proxima\UserBundle\Entity\User';
    }

    public function reloadUser(UserInterface $user)
    {
        $this->entityManager->refresh($user);
    }

    public function updateCanonicalFields(UserInterface $user)
    {
        $user->setUsernameCanonical($this->canonicalizeUsername($user->getUsername()));
        $user->setEmailCanonical($this->canonicalizeEmail($user->getEmail()));
    }

    public function updatePassword(UserInterface $user)
    {
        if (0 !== strlen($password = $user->getPlainPassword())) {
            $encoder = $this->getEncoder($user);
            $user->setPassword($encoder->encodePassword($password, $user->getSalt()));
            $user->eraseCredentials();
        }
    }

    public function updateUser(UserInterface $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
    

}
