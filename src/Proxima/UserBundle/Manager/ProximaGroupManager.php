<?php

/*
 * This file is part of the ProximaUserBundle package.
 * And implements FOSUserBundle package
 */

namespace Proxima\UserBundle\Manager;

use \FOS\UserBundle\Model\GroupManagerInterface;
use \FOS\UserBundle\Model\GroupInterface;
use Doctrine\ORM\EntityManager;
/**
 * Abstract Group Manager implementation which can be used as base class for your
 * concrete manager.
 *
 * @author Christophe Coevoet <stof@notk.org>
 * @author Wajih OUERIEMI <w.ourimi@gmail.com> override
 */
class ProximaGroupManager implements GroupManagerInterface
{
    /**
     * @var EntityManager 
     */
    protected $entityManager;
    
    /**
     * Constructor.
     *
     * @param EntityManager $entityManager
     * @param CanonicalizerInterface  $usernameCanonicalizer
     * @param CanonicalizerInterface  $emailCanonicalizer
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    /**
     * {@inheritDoc}
     */
    public function createGroup($name)
    {
        $class = $this->getClass();

        return new $class($name);
    }
    /**
     * {@inheritDoc}
     */
    public function findGroupByName($name)
    {
        return $this->findGroupBy(array('name' => $name));
    }
    
    public function deleteGroup(GroupInterface $group)
    {
        $this->entityManager->remove($group);
    }

    public function findGroupBy(array $criteria = null)
    {
        $criteria_index_tab = array_keys($criteria);
        $criteria_index     = $criteria_index_tab[0];
        $qb = $this->entityManager->createQueryBuilder()
              ->select('g')
              ->from($this->getClass(), 'g');
              if($criteria){
                $qb ->where(sprintf('g.%s = :criteria',$criteria_index))
                    ->setParameter('criteria', $criteria[$criteria_index]);
                return $qb->getQuery()->getSingleResult();
              }
        return $qb->getQuery()->getSingleResult();
    }

    public function findGroups()
    {
        return $this->findGroupBy(null);
    }

    public function getClass()
    {
        return 'Proxima\UserBundle\Entity\Group';
    }

    public function updateGroup(GroupInterface $group)
    {
        $this->entityManager->persist($group);
        $this->entityManager->flush();
    }

}
