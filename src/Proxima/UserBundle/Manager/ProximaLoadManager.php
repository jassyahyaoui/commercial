<?php

/*
 * This file is part of the ProximaUserBundle package.
 * And implements FOSUserBundle package
 */

namespace Proxima\UserBundle\Manager;

use Doctrine\ORM\EntityManager;
/**
 * Abstract Group Manager implementation which can be used as base class for your
 * concrete manager.
 *
 * @author Christophe Coevoet <stof@notk.org>
 * @author Ahmed HACHENA <a.hachena@gmail.com> override
 */
class ProximaloadManager 
{
    /**
     * @var EntityManager 
     */
    protected $entityManager;
    
    /**
     * Constructor.
     *
     * @param EntityManager $entityManager
     * @param CanonicalizerInterface  $usernameCanonicalizer
     * @param CanonicalizerInterface  $emailCanonicalizer
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    /**
     * {@inheritDoc}
     */
    public function createCaisse($name, $solde)
    {
        $class = $this->getClass();
        $caisse = new $class();
        $caisse->setName($name);
        $caisse->setSolde($solde);
        $this->entityManager->persist($caisse);
        $this->entityManager->flush();
        return $caisse;
    }
    
    public function getClass()
    {
        return 'Commercial\CoreBundle\Entity\Caisse';
    }

}