<?php

namespace Proxima\UserBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Proxima\UserBundle\Entity\User;
use Proxima\UserBundle\Form\UserType;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
     * List users
     * 
     */
    public function indexAction($page)
    {

        if (!$this->container->get('security.context')->isGranted('ROLE_ADMIN'))
        {
            throw new NotFoundHttpException('commercial Not found');
        }
        /* @var $userManager \Proxima\UserBundle\Manager\ProximaUserManager */
        $userManager = $this->container->get('proxima_user.manager');
        $entities    = $userManager->findUsers();

        return $this->render('ProximaUserBundle:User:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new User entity.
     *
     */
    public function createAction(Request $request)
    {
        if (!$this->container->get('security.context')->isGranted('ROLE_ADMIN'))
        {
            throw new NotFoundHttpException('commercial Not found');
        }

        /* @var $userManager \Proxima\UserBundle\Manager\ProximaUserManager */
        $userManager = $this->container->get('proxima_user.manager');

        $entity = $userManager->createUser();
        $form   = $this->createForm(new UserType($this->getUser()), $entity);
        $form->bind($request);

        if ($form->isValid())
        {
            $entity->setFullname();
            $entity->setCreatedBy($this->getUser());
            $userManager->updateUser($entity);
            $flash = array(
                'key'   => 'msg done',
                'title' => 'Succès',
                'msg'   => "L'utilisateur " . $entity->getFullname() . " a été créer avec succès ");
            $this->setFlash($flash);

            return $this->redirect($this->generateUrl('proxima_user_all'));
        }
        $group_widget = true;
        return $this->render('ProximaUserBundle:User:new.html.twig', array(
                    'entity'       => $entity,
                    'form'         => $form->createView(),
                    'group_widget' => $group_widget
        ));
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction()
    {
        if (!$this->container->get('security.context')->isGranted('ROLE_ADMIN'))
        {
            throw new NotFoundHttpException('commercial Not found');
        }

        $entity       = new User();
        $form         = $this->createForm(new UserType($this->getUser()), $entity);
        $group_widget = true;
        return $this->render('ProximaUserBundle:User:new.html.twig', array(
                    'entity'       => $entity,
                    'form'         => $form->createView(),
                    'group_widget' => $group_widget
        ));
    }

    public function checkAvailabilityByUserNameAction(Request $req)
    {
        if ($req->getMethod() == 'POST')
        {
            $username = $req->get('username');
            if ($username == "" || strlen($username) < 5)
            {
                return new JsonResponse(array('available' => false, 'isCheckable' => false));
            }
            /* @var $userManager \Proxima\UserBundle\Manager\ProximaUserManager */
            $userManager = $this->container->get('proxima_user.manager');
            if ($userManager->findUserByUsername($username))
            {
                return new JsonResponse(array('available' => false, 'isCheckable' => true));
            }

            return new JsonResponse(array('available' => true, 'isCheckable' => true));
        }
    }

    /**
     * Createing the flash message
     * 
     */
    protected function setFlash($value)
    {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

}
