<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use Commercial\CoreBundle\Entity\WarehouseArticle;
use Commercial\CoreBundle\Entity\WarehouseEntry;
use Commercial\CoreBundle\Entity\Reception;
use Commercial\CoreBundle\Entity\Transaction;
use Commercial\CoreBundle\Entity\ReceptionArticle;
use Commercial\CoreBundle\Form\ReceptionType;

class ReceptionController extends Controller
{
    public function addAction(Request $request)
    {
        $warehousearticle = new WarehouseArticle();
        $reception = new Reception();
        $reception_article = new ReceptionArticle();
        $reception->addReceptionArticle($reception_article);
        $form = $this->createForm(new ReceptionType(), $reception);
        
        if ($request->isMethod('POST')) {
            
            $form->bind($request);
            $em = $this->getDoctrine()->getManager();
            $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
            $total = 0;
            foreach ($reception->getReceptionArticles() as $reception_article)
            {
                $warehouse_entry = new WarehouseEntry();
                $transaction = new Transaction();
                $reception_article->setReception($reception);
                $em->persist($reception_article);
                $wa = $em->getRepository('CommercialCoreBundle:WarehouseArticle')
                     ->findOneBy(['warehouse' => $reception->getWarehouse(),
                                  'article'   => $reception_article->getArticle()]);
                if($wa ==null){
                    if($reception->getBlack()){
                        $warehousearticle->setQuantityBlack($reception_article->getQuantity());
                        $warehouse_entry->setBlack(true); }
                    else{
                        $warehousearticle->setQuantity($reception_article->getQuantity());
                        $warehouse_entry->setBlack(false); }
                    $warehousearticle->setWarehouse($reception->getWarehouse());
                    $warehousearticle->setArticle($reception_article->getArticle());
                    $warehousearticle->setSupplier($reception->getSupplier());
                    $em->persist($warehousearticle);
                    $warehouse_entry->setWarehouseArticle($warehousearticle);
                    $warehouse_entry->setUser($this->getUser());
                    $warehouse_entry->setQuantity($reception_article->getQuantity());
                    $warehouse_entry->setAmount($warehouse_entry->getQuantity() * ($warehousearticle->getArticle()->getBuyPrice()+($warehousearticle->getArticle()->getBuyPrice()*(($warehousearticle->getArticle()->getTva()->getTaux())/100))));
                }else{
                    $warehouse_entry->setQuantity($reception_article->getQuantity());
                    if($reception->getBlack()){
                        $wa->setQuantityBlack($wa->getQuantityBlack()+$warehouse_entry->getQuantity());
                        $warehouse_entry->setBlack(true); }
                    else{
                         $wa->setQuantity($wa->getQuantity()+$warehouse_entry->getQuantity());
                         $warehouse_entry->setBlack(false); }
                    $warehouse_entry->setAmount($warehouse_entry->getQuantity() * ($wa->getArticle()->getBuyPrice()+($wa->getArticle()->getBuyPrice()*(($wa->getArticle()->getTva()->getTaux())/100))));
                    $warehouse_entry->setWarehouseArticle($wa);
                    $warehouse_entry->setUser($this->getUser());
                }
                $em->persist($warehouse_entry);
                $transaction->setCaisse($caisse);
                $transaction->setType(1);   //depence
                $transaction->setUser($this->getUser());
                $transaction->setAmount($warehouse_entry->getAmount());
                $transaction->setDescription('achat de '.$reception_article->getQuantity().' '.$reception_article->getArticle()->getName());
                $em->persist($transaction);

                $solde = $caisse->getSolde();
                $caisse->setSolde($solde + $transaction->getAmount());
                
                
                
                $total = $total + (($reception_article->getArticle()->getBuyPrice()+((($reception_article->getArticle()->getBuyPrice()*$reception_article->getArticle()->getTva()->getTaux())/100))) * $reception_article->getQuantity());
            }
            $reception->setUser($this->getUser());
            $reception->setTotal($total);
            $em->persist($reception);

            
            
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"Le stock a été charger");
            $this->setFlash($flash);
            
            return $this->redirect($this->generateUrl('commercial_main_warehouseentry_all'));
        }
        
        return $this->render('CommercialMainBundle:Reception:add.html.twig', array(
            'form'    => $form->createView(),
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $reception = $em->getRepository('CommercialCoreBundle:Reception')->find($id);
        
        if(empty($reception)) {
            throw $this->createNotFoundException('Unable to find reception entity');
        }
        
        return $this->render('CommercialMainBundle:Reception:show.html.twig',array(
            'entity'    => $reception
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $reception = $em->getRepository('CommercialCoreBundle:Reception')->find($id);
        
        if(empty($reception)) {
            throw $this->createNotFoundException('Unable to find invoice entity');
        }
        
        $old_amount = $reception->getTotal();
        
        // original invoice articles
        $originalIps = array();

        foreach ($reception->getReceptionArticles() as $reception_article){
            $originalIps[] =$reception_article;
        }  
        
        $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
        $form = $this->createForm(new ReceptionType(), $reception);
        
        if ($request->isMethod('POST')) {
            $reception_data = $request->get('commercial_corebundle_invoice');
            $reception_data['payment_type'] = $request->get('payment_type');
            $reception_data['status'] = $request->get('status');
            
            $form->bind($reception_data);
            $total = 0;
            foreach ($reception->getReceptionArticles() as $reception_article){
                $reception_article->setReception($reception);
                $total = $total + $reception_article->getArticle()->getSellPrice() * $reception_article->getQuantity();
                foreach ($originalIps as $key => $toDel){
                    if($toDel->getId() === $reception_article->getId())
                        unset ($originalIps[$key]);
                }
            }
            
            foreach ($originalIps as $reception_article){
                $em->remove($reception_article);
            }
            
            if($reception->getDiscount() == NULL)
                $reception->setDiscount (0);
            $reception->setTotal($total-(($total*$reception->getDiscount())/100));
            
            $solde = $caisse->getSolde();
            $caisse->setSolde($solde + $reception->getTotal() - $old_amount);
            
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"La facture N° ".$reception->getNum()." a été modifier");
            $this->setFlash($flash);

            return $this->redirect($this->generateUrl('commercial_main_invoice_show', ['id'=>$reception->getId()]));
        }
        
        return $this->render('CommercialMainBundle:Reception:update.html.twig',array(
            'form'  => $form->createView(),
            'entity'=> $reception
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $reception = $em->getRepository('CommercialCoreBundle:Reception')->find($id);
        
        if(empty($reception)){
            throw $this->createNotFoundException('Unable to find invoice entity');
        }
        
        if(!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        
        $em->remove($reception);
        $em->flush();
        
        $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"La facture N° ".$reception->getNum()." a été supprimer");
                $this->setFlash($flash);
        
        return $this->redirect($this->generateUrl('commercial_main_invoice_all'));
    }

/**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}
