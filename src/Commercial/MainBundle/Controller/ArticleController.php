<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use Commercial\CoreBundle\Entity\Article;
use Commercial\CoreBundle\Entity\WarehouseArticle;
use Commercial\CoreBundle\Entity\WarehouseEntry;
use Commercial\CoreBundle\Entity\Transaction;
use Commercial\CoreBundle\Form\ArticleType;
use Commercial\CoreBundle\Form\WarehouseArticleType;
use Commercial\CoreBundle\Form\WarehouseEntryType;

class ArticleController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'reference', 'label' => 'Réference'],
            ['field'=>'name', 'label' => 'Désignation'],
            ['field'=>'sell_price', 'label' => 'Prix de vente HT'],
            ['field'=>'category', 'label' => 'Categorie'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');

        $entities = $em->getRepository('CommercialCoreBundle:Article')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Article')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        return $this->render('CommercialMainBundle:Article:index.html.twig',array(
              'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'Article', $lastPage, 1, $entities, $params, 'table table-hover table-nomargin table-bordered')
        ));

    }

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'reference', 'label' => 'Réference'],
            ['field'=>'name', 'label' => 'Désignation'],
            ['field'=>'sell_price', 'label' => 'Prix de vente HT'],
            ['field'=>'category', 'label' => 'Categorie'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $entities = $em->getRepository('CommercialCoreBundle:Article')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Article')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'Article', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list'      => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }
    
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $term = $request->get('term');
        
        $res = $em->getRepository('CommercialCoreBundle:Article')->findLike($term);
        
        return new JsonResponse($res);
    }

    public function addAction(Request $request)
    {
        $article = new Article();
        $warehousearticle = new WarehouseArticle();
        $warehouse_entry = new WarehouseEntry();
        $transaction = new Transaction();
        
        $form_article = $this->createForm(new WarehouseArticleType(), $warehousearticle);
        $form_entry = $this->createForm(new WarehouseEntryType(), $warehouse_entry);
        $form = $this->createForm(new ArticleType(), $article);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
            $form_article->bind($request);
            $form_entry->bind($request);
            
            $em = $this->getDoctrine()->getManager();
            $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
            $em->persist($article);
            
            $warehousearticle->setArticle($article);
            $warehousearticle->setQuantity($warehouse_entry->getQuantity());
            $warehouse_entry->setWarehouseArticle($warehousearticle);
            $warehouse_entry->setUser($this->getUser());
            $warehouse_entry->setAmount($warehouse_entry->getQuantity() * $article->getBuyPrice());
            $solde = $caisse->getSolde();
            $caisse->setSolde($solde - $warehouse_entry->getAmount());
            //efectuer une transaction de depense
            $transaction->setCaisse($caisse);
            $transaction->setType(1);   //depense
            $transaction->setUser($this->getUser());
            $transaction->setAmount($warehouse_entry->getAmount());
            $transaction->setDescription('achat de '.$warehouse_entry->getQuantity().' '.$article->getName());
            $em->persist($warehousearticle);
            $em->persist($warehouse_entry);
            $em->persist($transaction);
            
            
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"L'article ".$article->getName()." a été créer");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl('commercial_main_article_all'));
        }
        
        return $this->render('CommercialMainBundle:Article:add.html.twig', array(
            'form'    => $form->createView(),
            'form_article'    => $form_article->createView(),
            'form_entry'    => $form_entry->createView()
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Article')->find($id);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find article entity');
        }
        
        return $this->render('CommercialMainBundle:Article:show.html.twig',array(
            'entity'    => $entity
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Article')->find($id);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find article entity');
        }
        
        $form = $this->createForm(new ArticleType(), $entity);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
        
            if ($form->isValid()) {
                $em->flush();
                $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"L'article ".$entity->getName()." a été modifier");
                $this->setFlash($flash);
                
                return $this->redirect($this->generateUrl('commercial_main_article_all'));
            }
        }
        
        return $this->render('CommercialMainBundle:Article:update.html.twig',array(
            'form'  => $form->createView(),
            'entity'=> $entity
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Article')->find($id);
        
        if(empty($entity)){
            throw $this->createNotFoundException('Unable to find article entity');
        }
        
        if(!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        
        $em->remove($entity);
        $em->flush();
        
        $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"Le article ".$entity->getName()." a été supprimer");
                $this->setFlash($flash);
        
        return $this->redirect($this->generateUrl('commercial_main_article_all'));
    }
    
    public function getPriceAction(Request $request)
    {
        $id = $request->get('id_article');
        
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Article')->find($id);
        $out_qt = $em->getRepository('CommercialCoreBundle:InvoiceArticle')->findQt($id);
        $in_qt = $em->getRepository('CommercialCoreBundle:Article')->findQt($id);
        
        return new JsonResponse(array(
            'price' => $entity->getSellPrice(),
            'tva' => $entity->getTva()->getTaux(),
            'qt'    => $in_qt - $out_qt
                ));
    }

    /**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}