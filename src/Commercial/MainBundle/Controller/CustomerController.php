<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use Commercial\CoreBundle\Entity\Customer;
use Commercial\CoreBundle\Entity\Invoice;
use Commercial\CoreBundle\Entity\Transaction;
use Commercial\CoreBundle\Entity\InvoiceArticle;
use Commercial\CoreBundle\Form\CustomerType;

class CustomerController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'company_name', 'label' => 'Nom de société'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'address', 'label' => 'Adresse'],
            ['field'=>'tel', 'label' => 'Tel'],
            ['field'=>'email', 'label' => 'Email'],
            ['field'=>'description', 'label' => 'Description'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');

        $entities = $em->getRepository('CommercialCoreBundle:Customer')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Customer')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        return $this->render('CommercialMainBundle:Customer:index.html.twig',array(
              'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'Customer', $lastPage, 1, $entities, $params, 'table table-hover table-nomargin table-bordered')
        ));

    }

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'company_name', 'label' => 'Nom de société'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'address', 'label' => 'Adresse'],
            ['field'=>'tel', 'label' => 'Tel'],
            ['field'=>'email', 'label' => 'Email'],
            ['field'=>'description', 'label' => 'Description'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $entities = $em->getRepository('CommercialCoreBundle:Customer')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Customer')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'Customer', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list'      => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }
    
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $term = $request->get('term');
        
        $res = $em->getRepository('CommercialCoreBundle:Customer')->findLike($term);
        
        return new JsonResponse($res);
    }

    public function addAction(Request $request)
    {
        $customer = new Customer();
        
        $form = $this->createForm(new CustomerType(), $customer);
        
        if ($request->isMethod('POST')) {
            $customer_data = $request->get('commercial_corebundle_customer');
            $customer_data['tva_type'] = $request->get('tva_type');
            
            $form->bind($customer_data);
            
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($customer);
                $em->flush();
                $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"Le client ".$customer->getName()." a été créer");
                $this->setFlash($flash);
                return $this->redirect($this->generateUrl('commercial_main_customer_show', [ 'id' => $customer->getId() ]));
            }
        }
        
        return $this->render('CommercialMainBundle:Customer:add.html.twig', array(
            'form'    => $form->createView(),
        ));
    }

    public function showAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Customer')->find($id);
        $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find customer entity');
        }
        if ($request->isMethod('POST')) {
            $invoice = new Invoice();
            $transaction = new Transaction();
            $data = $request->get('check');
            $total = 0;
            $discount = 0;
            $bl_ids = implode(',', $data);
            $bl_articles = $em->getRepository('CommercialCoreBundle:DeliveryArticle')->findArticles($bl_ids);
            foreach ($bl_articles as $bl_article){
                $invoice_article = new InvoiceArticle();
                $invoice_article->setArticle($em->getRepository('CommercialCoreBundle:Article')->find($bl_article['id']));
                $invoice_article->setQuantity($bl_article['qt']);
                $invoice_article->setInvoice($invoice);
                $em->persist($invoice_article);
            }
            foreach ($data as $bl_id) {
                $bl = $em->getRepository('CommercialCoreBundle:DeliveryOrder')->find($bl_id);
                $total = $total + $bl->getTotal();
                $discount = $discount + $bl->getDiscount();
            }
            
            $invoice->setCustomer($entity);
            $invoice->setInvoiceDate(new \DateTime());
            $invoice->setTotal($total);
            $invoice->setDiscount($discount);
            $invoice->setUser($this->getUser());
            $invoice->setPaid(0);
            $invoice->setPaymentType(0);
            $em->persist($invoice);

            $transaction->setCaisse($caisse);
            $transaction->setType(0);   //encaissement
            $transaction->setUser($this->getUser());
            $transaction->setAmount($invoice->getTotal());
            $transaction->setDescription('payement du facture N°:'.$invoice->getNum());
            $em->persist($transaction);

            $solde = $caisse->getSolde();
            $caisse->setSolde($solde + $transaction->getAmount());
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"Le(s) bon(s) de livraison sont facturé");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl('commercial_main_invoice_show', ['id'=>$invoice->getId()]));
        }
        $form = $this->createFormBuilder()->getForm();
        
        return $this->render('CommercialMainBundle:Customer:show.html.twig',array(
            'entity'    => $entity,
            'form' => $form->createView()
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Customer')->find($id);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find customer entity');
        }
        
        $form = $this->createForm(new CustomerType(), $entity);
        
        if ($request->isMethod('POST')) {
            $customer_data = $request->get('commercial_corebundle_customer');
            $customer_data['tva_type'] = $request->get('tva_type');
            $form->bind($customer_data);
            
            if ($form->isValid()) {
                $em->flush();
                $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"Le client ".$entity->getName()." a été modifier");
                $this->setFlash($flash);
                
                return $this->redirect($this->generateUrl('commercial_main_customer_show', [ 'id' => $id ]));
            }
        }
        
        return $this->render('CommercialMainBundle:Customer:update.html.twig',array(
            'form'  => $form->createView(),
            'entity'=> $entity
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Customer')->find($id);
        
        if(empty($entity)){
            throw $this->createNotFoundException('Unable to find customer entity');
        }
        
        if(!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        
        $em->remove($entity);
        $em->flush();
        
        $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"Le client ".$entity->getName()." a été supprimer");
                $this->setFlash($flash);
        
        return $this->redirect($this->generateUrl('commercial_main_customer_all'));
    }

/**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}