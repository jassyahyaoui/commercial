<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use Commercial\CoreBundle\Entity\Warehouse;
use Commercial\CoreBundle\Form\WarehouseType;

class WarehouseController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemPerPage = $this->container->getParameter('itemPerPage');

        $params = [
            ['field'=>'id', 'label' => 'ID'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'address', 'label' => 'Adresse'],
            ['field'=>'description', 'label' => 'Description'],
            ['field'=>'tel', 'label' => 'Tel'],
            ['field'=>'created_at', 'label' => 'Date de creation']
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $entities = $em->getRepository('CommercialCoreBundle:Warehouse')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Warehouse')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        return $this->render('CommercialMainBundle:Warehouse:index.html.twig',array(
              'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'Warehouse', $lastPage, 1, $entities, $params, 'table table-hover table-nomargin table-bordered')
        ));

    }

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'id', 'label' => 'ID'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'address', 'label' => 'Adresse'],
            ['field'=>'description', 'label' => 'Description'],
            ['field'=>'tel', 'label' => 'Tel'],
            ['field'=>'created_at', 'label' => 'Date de creation']
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        $entities = $em->getRepository('CommercialCoreBundle:Warehouse')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Warehouse')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'Warehouse', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list'      => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }

    public function addAction(Request $request)
    {
        $warehouse = new Warehouse();
        
        $form = $this->createForm(new WarehouseType(), $warehouse);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($warehouse);
                $em->flush();
                $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"Le dépôt ".$warehouse->getName()." a été créer.");
                $this->setFlash($flash);
                return $this->redirect($this->generateUrl('commercial_main_warehouse_all'));
            }
        }
        
        return $this->render('CommercialMainBundle:Warehouse:add.html.twig', array(
            'form'    => $form->createView(),
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Warehouse')->find($id);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find warehouse entity');
        }
        
        return $this->render('CommercialMainBundle:Warehouse:show.html.twig',array(
            'entity'    => $entity
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Warehouse')->find($id);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find warehouse entity');
        }
        
        $form = $this->createForm(new WarehouseType(), $entity);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
        
            if ($form->isValid()) {
                $em->flush();
                $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"Le dépôt ".$entity->getName()." a été modifier");
                $this->setFlash($flash);
                
                return $this->redirect($this->generateUrl('commercial_main_warehouse_all'));
            }
        }
        
        return $this->render('CommercialMainBundle:Warehouse:update.html.twig',array(
            'form'  => $form->createView(),
            'entity'=> $entity
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Warehouse')->find($id);
        
        if(empty($entity)){
            throw $this->createNotFoundException('Unable to find warehouse entity');
        }
        
        if(!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        
        $em->remove($entity);
        $em->flush();
        $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"La dépôt ".$entity->getName()." a été supprimer");
                $this->setFlash($flash);
        
        return $this->redirect($this->generateUrl('commercial_main_warehouse_all'));
    }

/**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}
