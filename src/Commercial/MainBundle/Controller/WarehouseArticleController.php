<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class WarehouseArticleController extends Controller
{
    public function indexAction()
    {
        return $this->render('CommercialMainBundle:WarehouseArticle:stock.html.twig');

    }

    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->getRepository('CommercialCoreBundle:WarehouseArticle')->findArticles();

        $invoice_articles = $em->getRepository('CommercialCoreBundle:InvoiceArticle')->findQts();
        
        $delivery_articles = $em->getRepository('CommercialCoreBundle:DeliveryArticle')->findBlackQts();
        $res = NULL;
        foreach ($entities as $entity)
        {
            foreach ($invoice_articles as $ia)
            {
                if($entity['id']== $ia['id']) {
                    $entity['out_qte'] = $ia['out_qte'];
                    $entity['qte'] = $entity['in_qte']-$ia['out_qte'];
                }
            }
            foreach ($delivery_articles as $da)
            {
                if($entity['id']== $da['id']) {
                    $entity['qte_black'] = $entity['qte_black']-$da['black_qte'];
                }
            }
            if((!empty($entity['in_qte']))||(!empty($entity['qte_black'])))
                $res[] = $entity;
        }
        
        return new JsonResponse([
            'list'      => $res
        ]);
    }

/**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}