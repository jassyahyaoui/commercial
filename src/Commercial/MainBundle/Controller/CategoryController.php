<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use Commercial\CoreBundle\Entity\Category;
use Commercial\CoreBundle\Form\CategoryType;

class CategoryController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemPerPage = $this->container->getParameter('itemPerPage');

        $params = [
            ['field'=>'id', 'label' => 'ID'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'parent', 'label' => 'Parent'],
            ['field'=>'created_at', 'label' => 'Date de creation']
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $entities = $em->getRepository('CommercialCoreBundle:Category')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Category')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        return $this->render('CommercialMainBundle:Category:index.html.twig',array(
              'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'Category', $lastPage, 1, $entities, $params, 'table table-hover table-nomargin table-bordered')
        ));

    }

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'id', 'label' => 'ID'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'parent', 'label' => 'Parent'],
            ['field'=>'created_at', 'label' => 'Date de creation']
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        $entities = $em->getRepository('CommercialCoreBundle:Category')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Category')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'Category', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list'      => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }

    public function addAction(Request $request)
    {
        $category = new Category();
        
        $form = $this->createForm(new CategoryType(), $category);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"La famille ".$category->getName()." a été créer");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl('commercial_main_category_all'));
        }
        
        return $this->render('CommercialMainBundle:Category:add.html.twig', array(
            'form'    => $form->createView(),
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Category')->find($id);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find category entity');
        }
        
        return $this->render('CommercialMainBundle:Category:show.html.twig',array(
            'entity'    => $entity
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Category')->find($id);
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find category entity');
        }
        
        $form = $this->createForm(new CategoryType(), $entity, ['id' => $id]);
        if ($request->isMethod('POST')) {
            $form->bind($request);
        
            if ($form->isValid()) {
                $em->flush();
                $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"La famille ".$entity->getName()." a été modifier");
                $this->setFlash($flash);
                
                return $this->redirect($this->generateUrl('commercial_main_category_all'));
            }
        }
        
        return $this->render('CommercialMainBundle:Category:update.html.twig',array(
            'form'  => $form->createView(),
            'entity'=> $entity
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Category')->find($id);
        
        if(empty($entity)){
            throw $this->createNotFoundException('Unable to find category entity');
        }
        
        if(!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        
        $em->remove($entity);
        $em->flush();
        $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"La famille ".$entity->getName()." a été supprimer");
                $this->setFlash($flash);
        
        return $this->redirect($this->generateUrl('commercial_main_category_all'));
    }

/**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}
