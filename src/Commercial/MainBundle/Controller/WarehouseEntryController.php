<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use Commercial\CoreBundle\Entity\WarehouseArticle;
use Commercial\CoreBundle\Entity\WarehouseEntry;
use Commercial\CoreBundle\Entity\Transaction;
use Commercial\CoreBundle\Form\WarehouseArticleType;
use Commercial\CoreBundle\Form\WarehouseEntryType;

class WarehouseEntryController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemPerPage = $this->container->getParameter('itemPerPage');

        $params = [
            ['field'=>'reference', 'label' => 'Reference'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'category', 'label' => 'Categorie'],
            ['field'=>'quantity', 'label' => 'Quantité'],
            ['field'=>'black', 'label' => 'Type'],
            ['field'=>'buy_price', 'label' => 'Prix d\'achat HT'],
            ['field'=>'amount', 'label' => 'Montant HT']
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $entities = $em->getRepository('CommercialCoreBundle:WarehouseEntry')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:WarehouseEntry')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        return $this->render('CommercialMainBundle:WarehouseEntry:index.html.twig',array(
              'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'WarehouseEntry', $lastPage, 1, $entities, $params,'table table-hover table-nomargin table-colored-header')
        ));

    }

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'reference', 'label' => 'Reference'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'category', 'label' => 'Categorie'],
            ['field'=>'quantity', 'label' => 'Quantité'],
            ['field'=>'black', 'label' => 'Type'],
            ['field'=>'buy_price', 'label' => 'Prix d\'achat HT'],
            ['field'=>'amount', 'label' => 'Montant HT']
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        $entities = $em->getRepository('CommercialCoreBundle:WarehouseEntry')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:WarehouseEntry')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'WarehouseEntry', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list'      => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }

    public function addAction(Request $request)
    {
        $warehousearticle = new WarehouseArticle();
        $warehouse_entry = new WarehouseEntry();
        $transaction = new Transaction();
        
        $form = $this->createForm(new WarehouseArticleType(), $warehousearticle);
        $form_entry = $this->createForm(new WarehouseEntryType(), $warehouse_entry);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
            $form_entry->bind($request);

            $em = $this->getDoctrine()->getManager();
            $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
            $wa = $em->getRepository('CommercialCoreBundle:WarehouseArticle')
                     ->findOneBy(['warehouse' => $warehousearticle->getWarehouse(),
                                  'article'   => $warehousearticle->getArticle()]);
            $wa->setQuantity($wa->getQuantity()+$warehouse_entry->getQuantity());
            $warehouse_entry->setAmount($warehouse_entry->getQuantity() * $warehousearticle->getArticle()->getBuyPrice());
            $warehouse_entry->setWarehouseArticle($wa);
            $solde = $caisse->getSolde();
            $caisse->setSolde($solde - $warehouse_entry->getAmount());
            //efectuer une transaction de depense
            $transaction->setCaisse($em->getRepository('CommercialCoreBundle:Caisse')->find(1));
            $transaction->setType(1);   //depense
            $transaction->setUser($this->getUser());
            $transaction->setAmount($warehouse_entry->getAmount());
            $transaction->setDescription('achat de '.$warehouse_entry->getQuantity().' '.$warehousearticle->getArticle()->getName());
            $em->persist($transaction);
                
            $em->persist($warehouse_entry);
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"Le stock a été charger");
            $this->setFlash($flash);
            
            return $this->redirect($this->generateUrl('commercial_main_warehouseentry_all'));

        }
        
        return $this->render('CommercialMainBundle:WarehouseEntry:add.html.twig', array(
            'form'          => $form->createView(),
            'form_entry'    => $form_entry->createView()
        ));
    }
    
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:WarehouseEntry')->find($id);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find warehouseentry entity');
        }
        
        return $this->render('CommercialMainBundle:WarehouseEntry:show.html.twig',array(
            'entity'    => $entity
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $transaction = new Transaction();
        
        $em = $this->getDoctrine()->getManager();
        
        $warehouse_entry = $em->getRepository('CommercialCoreBundle:WarehouseEntry')->find($id);
        
        if(empty($warehouse_entry)) {
            throw $this->createNotFoundException('Unable to find warehouseentry entity');
        }
        
        $old_qt = $warehouse_entry->getQuantity();
        $old_amount = $warehouse_entry->getAmount();
        
        $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
        $form = $this->createForm(new WarehouseEntryType(), $warehouse_entry);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
            $new_qt = $warehouse_entry->getQuantity();
            $new_amount = $new_qt* $warehouse_entry->getWarehouseArticle()->getArticle()->getBuyPrice();
            $diff_amount = $new_amount - $old_amount;
            
            $wa = $em->getRepository('CommercialCoreBundle:WarehouseArticle')
                     ->findOneBy(['warehouse' => $warehouse_entry->getWarehouseArticle()->getWarehouse(),
                                  'article'   => $warehouse_entry->getWarehouseArticle()->getArticle()]);
            $wa->setQuantity($new_qt-$old_qt+ $wa->getQuantity());
            $warehouse_entry->setAmount($new_amount);
            
            $solde = $caisse->getSolde();
            $caisse->setSolde($solde - $diff_amount);
            
            $transaction->setCaisse($caisse);
            if ($diff_amount>0) {
                $transaction->setType(1);
            } else {
                $transaction->setType(0);
            }
                $transaction->setUser($this->getUser());
                $transaction->setAmount(abs($diff_amount));
                $transaction->setDescription('modification du mouvement d\'entrée N°:'.$warehouse_entry->getId());
                $em->persist($transaction);
            
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"L'entrée N° ".$warehouse_entry->getId()." a été modifier");
            $this->setFlash($flash);

            return $this->redirect($this->generateUrl('commercial_main_warehouseentry_all'));
        }
        
        return $this->render('CommercialMainBundle:WarehouseEntry:update.html.twig',array(
            'form'    => $form->createView(),
            'entity'=> $warehouse_entry
        ));
    }

    public function deleteAction($id)
    {
        $transaction = new Transaction();
        
        $em = $this->getDoctrine()->getManager();
        
        $warehouse_entry = $em->getRepository('CommercialCoreBundle:WarehouseEntry')->find($id);
        $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
        
        if(empty($warehouse_entry)){
            throw $this->createNotFoundException('Unable to find warehouseentry entity');
        }
        
        if(!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        $wa = $em->getRepository('CommercialCoreBundle:WarehouseArticle')
                     ->findOneBy(['warehouse' => $warehouse_entry->getWarehouseArticle()->getWarehouse(),
                                  'article'   => $warehouse_entry->getWarehouseArticle()->getArticle()]);
        if($warehouse_entry->getBlack()){
            $wa->setQuantityBlack($wa->getQuantityBlack()-$warehouse_entry->getQuantity());
        } else {
            $wa->setQuantity($wa->getQuantity()-$warehouse_entry->getQuantity());
        }
        
        $solde = $caisse->getSolde();
        $caisse->setSolde($solde + $warehouse_entry->getAmount());
        $transaction->setCaisse($em->getRepository('CommercialCoreBundle:Caisse')->find(1));
        $transaction->setType(0);   //encaissement
        $transaction->setUser($this->getUser());
        $transaction->setAmount($warehouse_entry->getAmount());
        $transaction->setDescription('annulation du mouvement d\'entrée N°:'.$warehouse_entry->getId());
        $em->persist($transaction);
        $em->remove($warehouse_entry);
        $em->flush();
        
        $flash= array(
            'key'=>'success',
            'title' => 'Succès',
            'msg'=>"L'entrée N° ".$warehouse_entry->getId()." a été supprimer");
        $this->setFlash($flash);
            
        return $this->redirect($this->generateUrl('commercial_main_warehouseentry_all'));
    }
    
/**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}
