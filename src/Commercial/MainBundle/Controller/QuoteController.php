<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Commercial\CoreBundle\Common\Util\Fpdf\PDFQuote;
use Commercial\CoreBundle\Entity\Quote;
use Commercial\CoreBundle\Entity\QuoteArticle;
use Commercial\CoreBundle\Entity\Invoice;
use Commercial\CoreBundle\Entity\Transaction;
use Commercial\CoreBundle\Entity\InvoiceArticle;
use Commercial\CoreBundle\Form\QuoteType;

class QuoteController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $itemPerPage = $this->container->getParameter('itemPerPage');

        $params = [
            ['field' => 'num', 'label' => 'Numéro'],
            ['field' => 'quote_date', 'label' => 'Date de sortie'],
            ['field' => 'total', 'label' => 'Total'],
            ['field' => 'cName', 'label' => 'Client'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');

        $entities = $em->getRepository('CommercialCoreBundle:Quote')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Quote')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        return $this->render('CommercialMainBundle:Quote:index.html.twig', array(
                    'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'Quote', $lastPage, 1, $entities, $params, 'table table-hover table-nomargin table-bordered')
        ));
    }

    public function listAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');

        $params = [
            ['field' => 'num', 'label' => 'Numéro'],
            ['field' => 'quote_date', 'label' => 'Date de sortie'],
            ['field' => 'total', 'label' => 'Total'],
            ['field' => 'cName', 'label' => 'Client'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');

        $entities = $em->getRepository('CommercialCoreBundle:Quote')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Quote')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'Quote', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list' => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }

    public function searchAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $term = $request->get('term');

        $res = $em->getRepository('CommercialCoreBundle:Quote')->findLike($term);

        return new JsonResponse($res);
    }

    public function addAction(Request $request) {
        $quote = new Quote();
        $quote_article = new QuoteArticle();
        $quote->addQuoteArticle($quote_article);
        $form = $this->createForm(new QuoteType(), $quote);

        if ($request->isMethod('POST')) {

            $form->bind($request);
            $em = $this->getDoctrine()->getManager();
            $total = 0;
            foreach ($quote->getQuoteArticles() as $quote_article) {
                if ($quote->getCustomer()->getTvaType() == 0) {
                    $total = $total + $quote_article->getArticle()->getSellPrice() * $quote_article->getQuantity();
                } else {
                    $total = $total + (($quote_article->getArticle()->getSellPrice() + ($quote_article->getArticle()->getSellPrice() * (($quote_article->getArticle()->getTva()->getTaux()) / 100))) * $quote_article->getQuantity());
                }
                $quote_article->setQuote($quote);
                $em->persist($quote_article);
            }
            $quote->setTotal($total - (($total * $quote->getDiscount()) / 100));
            $quote->setUser($this->getUser());
            $em->persist($quote);

            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "le devis N° " . $quote->getNum() . " a été créer");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl('commercial_main_quote_show', ['id' => $quote->getId()]));
        }

        return $this->render('CommercialMainBundle:Quote:add.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $quote = $em->getRepository('CommercialCoreBundle:Quote')->find($id);

        if (empty($quote)) {
            throw $this->createNotFoundException('Unable to find quote entity');
        }

        return $this->render('CommercialMainBundle:Quote:show.html.twig', array(
                    'entity' => $quote
        ));
    }

    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $quote = $em->getRepository('CommercialCoreBundle:Quote')->find($id);

        if (empty($quote)) {
            throw $this->createNotFoundException('Unable to find quote entity');
        }

        // original quote articles
        $originalIps = array();

        foreach ($quote->getQuoteArticles() as $quote_article) {
            $originalIps[] = $quote_article;
        }

        $form = $this->createForm(new QuoteType(), $quote);

        if ($request->isMethod('POST')) {
            $form->bind($request);
            $total = 0;
            foreach ($quote->getQuoteArticles() as $quote_article) {
                $quote_article->setQuote($quote);
                if ($quote->getCustomer()->getTvaType() == 0) {
                    $total = $total + $quote_article->getArticle()->getSellPrice() * $quote_article->getQuantity();
                } else {
                    $total = $total + (($quote_article->getArticle()->getSellPrice() + ($quote_article->getArticle()->getSellPrice() * (($quote_article->getArticle()->getTva()->getTaux()) / 100))) * $quote_article->getQuantity());
                }
                foreach ($originalIps as $key => $toDel) {
                    if ($toDel->getId() === $quote_article->getId())
                        unset($originalIps[$key]);
                }
            }

            foreach ($originalIps as $quote_article) {
                $em->remove($quote_article);
            }
            $quote->setTotal($total - (($total * $quote->getDiscount()) / 100));

            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "le devis N° " . $quote->getNum() . " a été modifier");
            $this->setFlash($flash);

            return $this->redirect($this->generateUrl('commercial_main_quote_show', ['id' => $quote->getId()]));
        }

        return $this->render('CommercialMainBundle:Quote:update.html.twig', array(
                    'form' => $form->createView(),
                    'entity' => $quote
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $quote = $em->getRepository('CommercialCoreBundle:Quote')->find($id);

        if (empty($quote)) {
            throw $this->createNotFoundException('Unable to find quote entity');
        }

        if (!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        $em->remove($quote);
        $em->flush();

        $flash = array(
            'key' => 'success',
            'title' => 'Succès',
            'msg' => "le devis N° " . $quote->getNum() . " a été supprimer");
        $this->setFlash($flash);

        return $this->redirect($this->generateUrl('commercial_main_quote_all'));
    }

    public function generatePDFAction($id) {

        $em = $this->getDoctrine()->getManager();

        $invoice = $em->getRepository('CommercialCoreBundle:Quote')->find($id);

        if (empty($invoice)) {
            throw $this->createNotFoundException('Unable to find quote entity.');
        }

        $som = 0;
        $tva = 0;
        foreach ($invoice->getQuoteArticles() as $ip) {
            $qt = $ip->getQuantity();
            $sp = $ip->getArticle()->getSellPrice();
            $articles [] = ['1' => $ip->getArticle()->getName(), '2' => $qt, '3' => number_format($sp, 3, '.', ' '), '4' => number_format($qt * $sp, 3, '.', ' ')];
            $som+=$qt * $sp;
            $tva = $tva + $ip->getQuantity() * (($ip->getArticle()->getSellPrice() * $ip->getArticle()->getTva()->getTaux()) / 100);
        }
        $dis = ($som * $invoice->getDiscount()) / 100;
        // echo '<pre>';        \Doctrine\Common\Util\Debug::dump($discount); exit();

        $pdf = new PDFQuote();
        $img_url = 'bundles/commercialmain/images/logo_alrahma.png';
        $header = array('DESCRIPTION', 'QTE', 'PRIX U', 'PRIX TOTAL');
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->BasicTable($header, array(
            '0' => '',
            '1' => 'CLIENT : ' . $invoice->getCustomer()->getCompanyName(),
            '2' => $invoice->getCustomer()->getAddress(),
            '3' => 'C.TVA : ' . $invoice->getCustomer()->getCodeTva(),
                ), array(
            '0' => 'DATE :      ' . $invoice->getQuoteDate()->format('d-m-Y'),
            '1' => 'Num Facture :     ' . $invoice->getNum()
                ), $img_url, $articles, $som, $dis, $tva);

        $pdf->Output();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContent($response);
        return $response;
    }

    public function pdfAction($id) {
        $em = $this->getDoctrine()->getManager();
        $quote = $em->getRepository('CommercialCoreBundle:Quote')->find($id);
//        echo'<pre>';\Doctrine\Common\Util\Debug::dump($quote); exit();
        if (empty($quote)) {
            throw $this->createNotFoundException('Unable to find quote entity.');
        }

        return $this->render('CommercialMainBundle:Quote:pdf.html.twig', [
                    'entity' => $quote
        ]);
    }

    public function generateFactureAction($id) {
        $invoice = new Invoice();
        $transaction = new Transaction();

        $em = $this->getDoctrine()->getManager();
        $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
        $quote = $em->getRepository('CommercialCoreBundle:Quote')->find($id);
        foreach ($quote->getQuoteArticles() as $quote_article) {
            $invoice_article = new InvoiceArticle();
            $invoice_article->setArticle($quote_article->getArticle());
            $invoice_article->setQuantity($quote_article->getQuantity());
            $invoice_article->setDescription($quote_article->getDescription());
            $invoice_article->setInvoice($invoice);
            $em->persist($invoice_article);
        }
        $invoice->setCustomer($quote->getCustomer());
        $invoice->setInvoiceDate($quote->getQuoteDate());
        $invoice->setTotal($quote->getTotal());
        $invoice->setDiscount($quote->getDiscount());
        $invoice->setUser($this->getUser());
        $invoice->setPaymentType(0);
        $em->persist($invoice);

        $transaction->setCaisse($caisse);
        $transaction->setType(0);   //encaissement
        $transaction->setUser($this->getUser());
        $transaction->setAmount($invoice->getTotal());
        $transaction->setDescription('payement du facture N°:' . $invoice->getNum());
        $em->persist($transaction);

        $solde = $caisse->getSolde();
        $caisse->setSolde($solde + $transaction->getAmount());

        $em->flush();
        $flash = array(
            'key' => 'success',
            'title' => 'Succès',
            'msg' => "La facture N° " . $invoice->getNum() . " a été créer");
        $this->setFlash($flash);
        return $this->redirect($this->generateUrl('commercial_main_invoice_update', ['id' => $invoice->getId()]));
    }

    /**
     * Createing the flash message
     *
     */
    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

}
