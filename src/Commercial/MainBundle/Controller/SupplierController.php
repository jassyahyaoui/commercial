<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use Commercial\CoreBundle\Entity\Supplier;
use Commercial\CoreBundle\Form\SupplierType;

class SupplierController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'company_name', 'label' => 'Nom de société'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'address', 'label' => 'Adresse'],
            ['field'=>'tel', 'label' => 'Tel'],
            ['field'=>'email', 'label' => 'Email'],
            ['field'=>'description', 'label' => 'Description'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');

        $entities = $em->getRepository('CommercialCoreBundle:Supplier')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Supplier')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        return $this->render('CommercialMainBundle:Supplier:index.html.twig',array(
              'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'Supplier', $lastPage, 1, $entities, $params, 'table table-hover table-nomargin table-bordered')
        ));

    }

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'company_name', 'label' => 'Nom de société'],
            ['field'=>'name', 'label' => 'Nom'],
            ['field'=>'address', 'label' => 'Adresse'],
            ['field'=>'tel', 'label' => 'Tel'],
            ['field'=>'email', 'label' => 'Email'],
            ['field'=>'description', 'label' => 'Description'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $entities = $em->getRepository('CommercialCoreBundle:Supplier')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Supplier')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'Supplier', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list'      => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }
    
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $term = $request->get('term');
        
        $res = $em->getRepository('CommercialCoreBundle:Supplier')->findLike($term);
        
        return new JsonResponse($res);
    }

    public function addAction(Request $request)
    {
        $supplier = new Supplier();
        
        $form = $this->createForm(new SupplierType(), $supplier);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($supplier);
                $em->flush();
                $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"Le fournisseur ".$supplier->getName()." a été créer");
                $this->setFlash($flash);
                return $this->redirect($this->generateUrl('commercial_main_supplier_all'));
            }
        }
        
        return $this->render('CommercialMainBundle:Supplier:add.html.twig', array(
            'form'    => $form->createView(),
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Supplier')->find($id);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find supplier entity');
        }
        
        return $this->render('CommercialMainBundle:Supplier:show.html.twig',array(
            'entity'    => $entity
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Supplier')->find($id);
        
        if(empty($entity)) {
            throw $this->createNotFoundException('Unable to find supplier entity');
        }
        
        $form = $this->createForm(new SupplierType(), $entity);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
        
            if ($form->isValid()) {
                $em->flush();
                $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"Le fournisseur ".$entity->getName()." a été modifier");
                $this->setFlash($flash);
                
                return $this->redirect($this->generateUrl('commercial_main_supplier_all'));
            }
        }
        
        return $this->render('CommercialMainBundle:Supplier:update.html.twig',array(
            'form'  => $form->createView(),
            'entity'=> $entity
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CommercialCoreBundle:Supplier')->find($id);
        
        if(empty($entity)){
            throw $this->createNotFoundException('Unable to find supplier entity');
        }
        
        if(!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        
        $em->remove($entity);
        $em->flush();
        
        $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"Le fournisseur ".$entity->getName()." a été supprimer");
                $this->setFlash($flash);
        
        return $this->redirect($this->generateUrl('commercial_main_supplier_all'));
    }

/**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}
