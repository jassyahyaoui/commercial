<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use Commercial\CoreBundle\Common\Util\Fpdf\PDF;
use Commercial\CoreBundle\Entity\Invoice;
use Commercial\CoreBundle\Entity\Transaction;
use Commercial\CoreBundle\Entity\InvoiceArticle;
use Commercial\CoreBundle\Entity\Deadline;
use Commercial\CoreBundle\Form\InvoiceType;

class InvoiceController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'num', 'label' => 'Numéro'],
            ['field'=>'invoice_date', 'label' => 'Date de sortie'],
            ['field'=>'total', 'label' => 'Total'],
            ['field'=>'cName', 'label' => 'Client'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');

        $entities = $em->getRepository('CommercialCoreBundle:Invoice')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Invoice')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        return $this->render('CommercialMainBundle:Invoice:index.html.twig',array(
              'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'Invoice', $lastPage, 1, $entities, $params, 'table table-hover table-nomargin table-bordered')
        ));

    }

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'num', 'label' => 'Numéro'],
            ['field'=>'invoice_date', 'label' => 'Date de sortie'],
            ['field'=>'total', 'label' => 'Total'],
            ['field'=>'cName', 'label' => 'Client'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $entities = $em->getRepository('CommercialCoreBundle:Invoice')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Invoice')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'Invoice', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list'      => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }
    
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $term = $request->get('term');
        
        $res = $em->getRepository('CommercialCoreBundle:Invoice')->findLike($term);
        
        return new JsonResponse($res);
    }

    public function addAction(Request $request)
    {
        $invoice = new Invoice();
        $transaction = new Transaction();
        $invoice_article = new InvoiceArticle();
        $deadline = new Deadline();
        $invoice->addInvoiceArticle($invoice_article);
        $invoice->addDeadline($deadline);
        $form = $this->createForm(new InvoiceType(), $invoice);
        
        if ($request->isMethod('POST')) {
            $invoice_data = $request->get('commercial_corebundle_invoice');
            $invoice_data['payment_type'] = $request->get('payment_type');
            $form->bind($invoice_data);
            $em = $this->getDoctrine()->getManager();
            $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
            $total = 0;
            foreach ($invoice->getInvoiceArticles() as $invoice_article)
            {
                if($invoice->getCustomer()->getTvaType()==0) {
                    $total = $total + $invoice_article->getArticle()->getSellPrice() * $invoice_article->getQuantity();
                } else {
                    $total = $total + (($invoice_article->getArticle()->getSellPrice()+($invoice_article->getArticle()->getSellPrice()*(($invoice_article->getArticle()->getTva()->getTaux())/100))) * $invoice_article->getQuantity());
                }
                $invoice_article->setInvoice($invoice);
                $em->persist($invoice_article);
            }
            foreach ($invoice->getDeadlines() as $deadline)
            {
                $deadline->setInvoice($invoice);
                $em->persist($deadline);
            }
            $invoice->setTotal($total-(($total*$invoice->getDiscount())/100));
            $invoice->setUser($this->getUser());
            if($invoice->getPaid()== NULL){
                $invoice->setPaid(0);
                $em->persist($invoice);
            }else{
                $em->persist($invoice);
            }
            
            $transaction->setCaisse($em->getRepository('CommercialCoreBundle:Caisse')->find(1));
            $transaction->setType(0);   //encaissement
            $transaction->setUser($this->getUser());
            $transaction->setAmount($invoice->getTotal());
            $transaction->setDescription('payement du facture N°:'.$invoice->getNum());
            $em->persist($transaction);

            $solde = $caisse->getSolde();
            $caisse->setSolde($solde + $transaction->getAmount());
            
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"La facture N° ".$invoice->getNum()." a été créer");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl('commercial_main_invoice_show', ['id'=>$invoice->getId()]));
        }
        
        return $this->render('CommercialMainBundle:Invoice:add.html.twig', array(
            'form'    => $form->createView(),
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $invoice = $em->getRepository('CommercialCoreBundle:Invoice')->find($id);
        
        if(empty($invoice)) {
            throw $this->createNotFoundException('Unable to find invoice entity');
        }
        
        return $this->render('CommercialMainBundle:Invoice:show.html.twig',array(
            'entity'    => $invoice
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $transaction = new Transaction();
        
        $em = $this->getDoctrine()->getManager();
        
        $invoice = $em->getRepository('CommercialCoreBundle:Invoice')->find($id);
        
        if(empty($invoice)) {
            throw $this->createNotFoundException('Unable to find invoice entity');
        }
        
        $old_amount = $invoice->getTotal();
        
        // original invoice articles
        $originalIps = array();

        foreach ($invoice->getInvoiceArticles() as $invoice_article){
            $originalIps[] =$invoice_article;
        }
        
        // original invoice articles
        $originaldeadlines = array();

        foreach ($invoice->getDeadlines() as $deadline){
            $originaldeadlines[] =$deadline;
        }
        
        $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
        $solde = $caisse->getSolde();
        
        $form = $this->createForm(new InvoiceType(), $invoice);
        
        if ($request->isMethod('POST')) {
            $invoice_data = $request->get('commercial_corebundle_invoice');
            $invoice_data['payment_type'] = $request->get('payment_type');
            
            $form->bind($invoice_data);
            $total = 0;
            foreach ($invoice->getInvoiceArticles() as $invoice_article){
                $invoice_article->setInvoice($invoice);
                if($invoice->getCustomer()->getTvaType()==0) {
                    $total = $total + $invoice_article->getArticle()->getSellPrice() * $invoice_article->getQuantity();
                } else {
                    $total = $total + (($invoice_article->getArticle()->getSellPrice()+($invoice_article->getArticle()->getSellPrice()*(($invoice_article->getArticle()->getTva()->getTaux())/100))) * $invoice_article->getQuantity());
                }
                foreach ($originalIps as $key => $toDel){
                    if($toDel->getId() === $invoice_article->getId())
                        unset ($originalIps[$key]);
                }
            }
            
            foreach ($invoice->getDeadlines() as $deadline)
            {
                $deadline->setInvoice($invoice);
                foreach ($originaldeadlines as $key => $toDel){
                    if($toDel->getId() === $deadline->getId())
                        unset ($originaldeadlines[$key]);
                }
            }
//            echo '<pre>';            \Doctrine\Common\Util\Debug::dump($total); exit();
            foreach ($originalIps as $invoice_article){
                $em->remove($invoice_article);
            }
            foreach ($originaldeadlines as $deadline){
                $em->remove($deadline);
            }
            
            $invoice->setTotal($total-(($total*$invoice->getDiscount())/100));
            $diff_amount = $invoice->getTotal() - $old_amount;
            $caisse->setSolde($solde + $diff_amount);
            
            $transaction->setCaisse($caisse);
            if ($diff_amount>0) {
                $transaction->setType(0);
            } else {
                $transaction->setType(1);
            }
                $transaction->setUser($this->getUser());
                $transaction->setAmount(abs($diff_amount));
                $transaction->setDescription('modification du facture N°:'.$invoice->getNum());
                if($transaction->getAmount()>0)
                    $em->persist($transaction);
            
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"La facture N° ".$invoice->getNum()." a été modifier");
            $this->setFlash($flash);

            return $this->redirect($this->generateUrl('commercial_main_invoice_show', ['id'=>$invoice->getId()]));
        }
        
        return $this->render('CommercialMainBundle:Invoice:update.html.twig',array(
            'form'  => $form->createView(),
            'entity'=> $invoice
        ));
    }

    public function deleteAction($id)
    {
        $transaction = new Transaction();
        
        $em = $this->getDoctrine()->getManager();
        
        $invoice = $em->getRepository('CommercialCoreBundle:Invoice')->find($id);
        $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
        
        if(empty($invoice)){
            throw $this->createNotFoundException('Unable to find invoice entity');
        }
        
        if(!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        $solde = $caisse->getSolde();
        $caisse->setSolde($solde - $invoice->getTotal());
        $transaction->setCaisse($caisse);
        $transaction->setType(1);   //depense
        $transaction->setUser($this->getUser());
        $transaction->setAmount($invoice->getTotal());
        $transaction->setDescription('annulation du facture N°:'.$invoice->getNum());
        $em->persist($transaction);
        
        $em->remove($invoice);
        $em->flush();
        
        $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"La facture N° ".$invoice->getNum()." a été supprimer");
                $this->setFlash($flash);
        
        return $this->redirect($this->generateUrl('commercial_main_invoice_all'));
    }
    
    public function generatePDFAction($id) {
        $em = $this->getDoctrine()->getManager();

        $invoice = $em->getRepository('CommercialCoreBundle:Invoice')->find($id);

        if (empty($invoice)) {
            throw $this->createNotFoundException('Unable to find invoice entity.');
        }

        $som = 0;
        $tva = 0;
        foreach ($invoice->getInvoiceArticles() as $ip) {
            $qt = $ip->getQuantity();
            $sp = $ip->getArticle()->getSellPrice();
            $articles [] = ['1' => $ip->getArticle()->getName(), '2' => $qt, '3' => number_format($sp, 3, '.', ' '), '4' => number_format($qt * $sp, 3, '.', ' ')];
            $som+=$qt * $sp;
            $tva = $tva + $ip->getQuantity()*(($ip->getArticle()->getSellPrice()*$ip->getArticle()->getTva()->getTaux())/100);
        }
        $dis = ($som * $invoice->getDiscount()) / 100;
        // echo '<pre>';        \Doctrine\Common\Util\Debug::dump($discount); exit();

        $pdf = new PDF();
        $img_url = 'bundles/commercialmain/images/logo_alrahma.png';
        $header = array('DESCRIPTION', 'QTE', 'PRIX U', 'PRIX TOTAL');
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->BasicTable($header, array(
            '0' => '',
            '1' => 'CLIENT : ' . $invoice->getCustomer()->getCompanyName(),
            '2' => $invoice->getCustomer()->getAddress(),
            '3' => 'C.TVA : ' . $invoice->getCustomer()->getCodeTva(),
                ), array(
            '0' => 'DATE :      ' . $invoice->getInvoiceDate()->format('d-m-Y'),
            '1' => 'Num Facture :     ' . $invoice->getNum()
                ), $img_url, $articles, $som, $dis, $tva);

        $pdf->Output();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContent($response);
        return $response;
    }
    
    public function pdfAction($id) {
        $em = $this->getDoctrine()->getManager();
        $invoice = $em->getRepository('CommercialCoreBundle:Invoice')->find($id);
//        echo'<pre>';\Doctrine\Common\Util\Debug::dump($invoice); exit();
        if (empty($invoice)) {
            throw $this->createNotFoundException('Unable to find invoice entity.');
        }
        
        return $this->render('CommercialMainBundle:Invoice:pdf.html.twig', [
                    'entity' => $invoice
        ]);
    }

/**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}
