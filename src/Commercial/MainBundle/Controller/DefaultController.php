<?php

namespace Commercial\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
        $invoices = $em->getRepository('CommercialCoreBundle:Invoice')->findAll();
        $customers = $em->getRepository('CommercialCoreBundle:Customer')->findAll();
        $last_invoices = $em->getRepository('CommercialCoreBundle:Invoice')->findLast();
        $invoiceArticles = $em->getRepository('CommercialCoreBundle:InvoiceArticle')->findList();
//        echo '<pre>'; \Doctrine\Common\Util\Debug::dump($invoiceArticles); exit();
        $nb_invoices = count($invoices);
        $nb_customers = count($customers);
        $nb_ips = count($invoiceArticles);
        $solde = $caisse->getSolde();
        return $this->render('CommercialMainBundle:Default:index.html.twig', array(
            'solde'          => $solde,
            'nb_invoices'    => $nb_invoices,
            'nb_customers'   => $nb_customers,
            'last_invoices'  => $last_invoices,
            'invoiceArticles'=> $invoiceArticles,
            'nb_ips'         => $nb_ips
            ));
    }
    
    public function deadlineAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $deadlines = $em->getRepository('CommercialCoreBundle:Deadline')->findDeadlines();
        $n = count($deadlines);
        if($n == 0)
            $n= '';
        
        return new JsonResponse([
            'n'         => $n,
            'deadlines' => $this->renderView('CommercialMainBundle:Default:deadlines.html.twig', [
                                'deadlines' =>$deadlines
                            ])
        ]);
    }
}
