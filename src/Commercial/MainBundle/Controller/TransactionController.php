<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Commercial\CoreBundle\Entity\Transaction;
use Commercial\CoreBundle\Form\TransactionType;

class TransactionController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'id', 'label' => 'Id'],
            ['field'=>'created_at', 'label' => 'Date'],
            ['field'=>'t_type', 'label' => 'Type'],
            ['field'=>'amount', 'label' => 'Montant'],
            ['field'=>'description', 'label' => 'Description'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
        $solde = $caisse->getSolde();

        $entities = $em->getRepository('CommercialCoreBundle:Transaction')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Transaction')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        return $this->render('CommercialMainBundle:Transaction:index.html.twig',array(
              'solde'          => $solde,
              'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'Transaction', $lastPage, 1, $entities, $params, 'table table-hover table-nomargin table-bordered')
        ));

    }

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'id', 'label' => 'Id'],
            ['field'=>'created_at', 'label' => 'Date'],
            ['field'=>'t_type', 'label' => 'Type'],
            ['field'=>'amount', 'label' => 'Montant'],
            ['field'=>'description', 'label' => 'Description'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $entities = $em->getRepository('CommercialCoreBundle:Transaction')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:Transaction')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'Transaction', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list'      => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }
    
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $term = $request->get('term');
        
        $res = $em->getRepository('CommercialCoreBundle:Transaction')->findLike($term);
        
        return new JsonResponse($res);
    }

    public function addAction(Request $request)
    {
        $transaction = new Transaction();
        
        $form = $this->createForm(new TransactionType(), $transaction);
        
        if ($request->isMethod('POST')) {
            $transaction_data = $request->get('commercial_corebundle_transaction');
            $transaction_data['type'] = $request->get('transaction_type');
            $form->bind($transaction_data);
                
            $em = $this->getDoctrine()->getManager();
            $caisse = $em->getRepository('CommercialCoreBundle:Caisse')->find(1);
            $transaction->setCaisse($caisse);
            $transaction->setUser($this->getUser());
            $em->persist($transaction);
            
            $solde = $caisse->getSolde();
            if($transaction_data['type']==0) {
                $caisse->setSolde($solde + $transaction->getAmount());
                $transaction->setType(0);
            } else{
                $caisse->setSolde($solde - $transaction->getAmount());
                $transaction->setType(1);
            }
            
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"La transaction N°: ".$transaction->getId()." a été créer");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl('commercial_main_transaction_all'));
        }
        
        return $this->render('CommercialMainBundle:Transaction:add.html.twig', array(
            'form'    => $form->createView(),
        ));
    }

     /**
    * Createing the flash message
    *
    */
    protected function setFlash($value) {
    $this->container->get('session')->getFlashBag()->add('alert', $value);
    }
}