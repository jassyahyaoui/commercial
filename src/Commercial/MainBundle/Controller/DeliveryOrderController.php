<?php

namespace Commercial\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use Commercial\CoreBundle\Entity\DeliveryOrder;
use Commercial\CoreBundle\Entity\DeliveryArticle;
use Commercial\CoreBundle\Form\DeliveryOrderType;

class DeliveryOrderController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'num', 'label' => 'Numéro'],
            ['field'=>'delivery_date', 'label' => 'Date de sortie'],
            ['field'=>'black', 'label' => 'Type'],
            ['field'=>'total', 'label' => 'Total'],
            ['field'=>'cName', 'label' => 'Client'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');

        $entities = $em->getRepository('CommercialCoreBundle:DeliveryOrder')->findList($itemPerPage, 1);
        $totalItems = count($em->getRepository('CommercialCoreBundle:DeliveryOrder')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);

        return $this->render('CommercialMainBundle:DeliveryOrder:index.html.twig',array(
              'table' => $manipulator->generateTable('Commercial', 'MainBundle', 'DeliveryOrder', $lastPage, 1, $entities, $params, 'table table-hover table-nomargin table-bordered')
        ));

    }

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $page = $request->get('page');

        $itemPerPage = $this->container->getParameter('itemPerPage');
        
        $params = [
            ['field'=>'num', 'label' => 'Numéro'],
            ['field'=>'delivery_date', 'label' => 'Date de sortie'],
            ['field'=>'black', 'label' => 'Type'],
            ['field'=>'total', 'label' => 'Total'],
            ['field'=>'cName', 'label' => 'Client'],
        ];
        $manipulator = $this->container->get('proxima_table.manipulator');
        
        $entities = $em->getRepository('CommercialCoreBundle:DeliveryOrder')->findList($itemPerPage, $page);
        $totalItems = count($em->getRepository('CommercialCoreBundle:DeliveryOrder')->findAll());
        $lastPage = ceil($totalItems / $itemPerPage);
        
        $res = $manipulator->paginateList('Commercial', 'MainBundle', 'DeliveryOrder', $lastPage, $page, $entities, $params);

        return new JsonResponse([
            'list'      => $res['list'],
            'paginator' => $res['paginator']
        ]);
    }

    public function addAction(Request $request)
    {
        $delivery = new DeliveryOrder();
        $delivery_article = new DeliveryArticle();
        $delivery->addDeliveryArticle($delivery_article);
        $form = $this->createForm(new DeliveryOrderType(), $delivery);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
            $em = $this->getDoctrine()->getManager();
            $total = 0;
            foreach ($delivery->getDeliveryArticles() as $delivery_article)
            {
                if($delivery->getCustomer()->getTvaType()==0) {
                    $total = $total + $delivery_article->getArticle()->getSellPrice() * $delivery_article->getQuantity();
                } else {
                    $total = $total + (($delivery_article->getArticle()->getSellPrice()+($delivery_article->getArticle()->getSellPrice()*(($delivery_article->getArticle()->getTva()->getTaux())/100))) * $delivery_article->getQuantity());
                }
                if($delivery->getBlack()) {
                    $delivery_article->setBlack(true);
                }
                $delivery_article->setDelivery($delivery);
                $em->persist($delivery_article);
            }
            $delivery->setTotal($total-(($total*$delivery->getDiscount())/100));
            $delivery->setUser($this->getUser());
            $em->persist($delivery);
            
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"La bon de livraison N° ".$delivery->getNum()." a été créer");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl('commercial_main_deliveryorder_show', ['id'=>$delivery->getId()]));
        }
        
        return $this->render('CommercialMainBundle:DeliveryOrder:add.html.twig', array(
            'form'    => $form->createView(),
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $delivery = $em->getRepository('CommercialCoreBundle:DeliveryOrder')->find($id);
        
        if(empty($delivery)) {
            throw $this->createNotFoundException('Unable to find delivery entity');
        }
        
        return $this->render('CommercialMainBundle:DeliveryOrder:show.html.twig',array(
            'entity'    => $delivery
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $delivery = $em->getRepository('CommercialCoreBundle:DeliveryOrder')->find($id);
        
        if(empty($delivery)) {
            throw $this->createNotFoundException('Unable to find delivery entity');
        }
        
        // original delivery articles
        $originalIps = array();

        foreach ($delivery->getDeliveryArticles() as $delivery_article){
            $originalIps[] =$delivery_article;
        }
        
        $form = $this->createForm(new DeliveryOrderType(), $delivery);
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
            $total = 0;
            foreach ($delivery->getDeliveryArticles() as $delivery_article){
                $delivery_article->setDelivery($delivery);
                if($delivery->getCustomer()->getTvaType()==0) {
                    $total = $total + $delivery_article->getArticle()->getSellPrice() * $delivery_article->getQuantity();
                } else {
                    $total = $total + (($delivery_article->getArticle()->getSellPrice()+($delivery_article->getArticle()->getSellPrice()*(($delivery_article->getArticle()->getTva()->getTaux())/100))) * $delivery_article->getQuantity());
                }
                foreach ($originalIps as $key => $toDel){
                    if($toDel->getId() === $delivery_article->getId())
                        unset ($originalIps[$key]);
                }
            }
            
            foreach ($originalIps as $delivery_article){
                $em->remove($delivery_article);
            }
            $delivery->setTotal($total-(($total*$delivery->getDiscount())/100));
            
            $em->flush();
            $flash= array(
                'key'=>'success',
                'title' => 'Succès',
                'msg'=>"La bon de livraison N° ".$delivery->getNum()." a été modifier");
            $this->setFlash($flash);

            return $this->redirect($this->generateUrl('commercial_main_deliveryorder_show', ['id'=>$delivery->getId()]));
        }
        
        return $this->render('CommercialMainBundle:DeliveryOrder:update.html.twig',array(
            'form'  => $form->createView(),
            'entity'=> $delivery
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $delivery = $em->getRepository('CommercialCoreBundle:DeliveryOrder')->find($id);
        
        if(empty($delivery)){
            throw $this->createNotFoundException('Unable to find delivery entity');
        }
        
        if(!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }
        
        $em->remove($delivery);
        $em->flush();
        
        $flash= array(
                    'key'=>'success',
                    'title' => 'Succès',
                    'msg'=>"La bon de livraison N° ".$delivery->getNum()." a été supprimer");
                $this->setFlash($flash);
        
        return $this->redirect($this->generateUrl('commercial_main_deliveryorder_all'));
    }
    
    public function generatePDFAction($id) {
        $em = $this->getDoctrine()->getManager();   
        
        $delivery = $em->getRepository('CommercialCoreBundle:DeliveryOrder')->find($id);

        if (empty($delivery)) {
            throw $this->createNotFoundException('Unable to find delivery entity.');
        }
        
        $pdf = $this->generateUrl('commercial_main_deliveryorder_pdf', array('id' => $id), true);
        
        return new Response(
                $this->get('knp_snappy.pdf')->getOutput($pdf), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="bl_'. $delivery->getNum() . '.pdf"'
        ]);
    }
    
    public function pdfAction($id) {
        $em = $this->getDoctrine()->getManager();
        $delivery = $em->getRepository('CommercialCoreBundle:DeliveryOrder')->find($id);
//        echo'<pre>';\Doctrine\Common\Util\Debug::dump($delivery); exit();
        if (empty($delivery)) {
            throw $this->createNotFoundException('Unable to find delivery entity.');
        }
        
        return $this->render('CommercialMainBundle:DeliveryOrder:pdf.html.twig', [
                    'entity' => $delivery
        ]);
    }

    /**
 * Createing the flash message
 *
 */
protected function setFlash($value) {
$this->container->get('session')->getFlashBag()->add('alert', $value);
}

}
