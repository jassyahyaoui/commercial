var Transaction = function(parameters) {
    var $__s = this,
            $__params = parameters;
    var obj = {
        page: {
            /**
             * initializer
             * @returns {undefined}
             */
            _init: function() {
                obj.page._bindList();
            },
            _bindList: function(){
              $(document).on('click', '.page-btn', function(e){
                  e.preventDefault();
                  if ($(this).attr('id') !== "") {
                    obj.list._showPage($(this).attr('id'));
                  }
              });
            }
        },
        list: {
            _showPage: function(page) {
                obj.list._paginate(page);
            },
            _paginate: function(page) {
                $.ajax({
                    url: Routing.generate('commercial_main_transaction_list'),
                    type: 'post',
                    data: {'page': page},
                    success: function(response) {
                        $.unblockpage();
                        $('#list tbody').html(response.list);
                        $('.table-pagination').html(response.paginator);
                    },
                    error: function() {
                        bootbox.hideAll();
                        $.alert({
                            msg: 'Server error please try again later.',
                            callback: function() {
                                bootbox.hideAll();
                            }
                        });
                    },
                    beforSend: function() {
                        $.blockpage();
                    }
                });
            }
        }
    };
    return {
        /**
         * intializer
         * @returns {undefined}
         */
        init: function() {
            obj.page._init();
        }
    };
};
Transaction.prototype = new EventDispatcher({}, 'transaction');