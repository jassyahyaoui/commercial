// Récupère le div qui contient la collection de items
var collectionHolder = $('div.items');

// ajoute un lien « add a offer »
var $addItemLink = $('<a href="#" class="add_item_link">+ Ajouter un article</a>');
var $newLinkTr = $('<div class="span2" style="float: right;"></div>').append($addItemLink);


$(document).ready(function() {
    // ajoute l'ancre « ajouter un offer » et tr à la balise ul
    collectionHolder.append($newLinkTr);
    
    $addItemLink.on('click', function(e) {
        // empêche le lien de créer un « # » dans l'URL
        e.preventDefault();

        // ajoute un nouveau formulaire offer (voir le prochain bloc de code)
        addItemForm(collectionHolder, $newLinkTr);
    });
    
    collectionHolder.find('.item').each(function() {
        addItemFormDeleteLink($(this));
    });
});

function addItemForm(collectionHolder, $newLinkTr) {
    // Récupère l'élément ayant l'attribut data-prototype comme expliqué plus tôt
    var prototype = collectionHolder.attr('data-prototype');

    // Remplace '__name__' dans le HTML du prototype par un nombre basé sur
    // la longueur de la collection courante
    var newForm = prototype.replace(/__name__/g, collectionHolder.children().length);

    // Affiche le formulaire dans la page dans un tr, avant le lien "ajouter un offer"
    var $newFormTr = $('<div class="item"></div>').append(newForm);
    $newLinkTr.before($newFormTr);
    addItemFormDeleteLink($newFormTr);
    $('.select2-me').select2();
    $('.grid-me .select2-me').on('change', function() {
            var id_article = $(this).val(),
                    id = $(this).attr('id');
            $.ajax({
                url: Routing.generate('commercial_main_article_get_price'),
                type: 'post',
                data: {'id_article': id_article},
                success: function(response) {
                    $('#' + id).closest('.span2').next().html(response.price.toFixed(3));
                    $('#' + id).closest('.span2').next().next().html(response.qt);
                }
            });
        });
    $('.qt').on('keyup', function(){
       var pu = $(this).parent().prev().prev().text(),
           qt = $(this).val(),
           total = 0;
       $(this).parent().next().html((pu*qt).toFixed(3));
       $('.qt').each(function(){
                total = parseFloat(total) + parseFloat($(this).parent().next().html());
              $('#total').html(total.toFixed(3));  
            });
   });
}

function addItemFormDeleteLink($offerFormTr) {
    var $removeFormA = $('<div class="span2" style="text-align: center;"><a class="btn delete" title="Effacer"><i class="icon-remove"></i></a></div>');
    $offerFormTr.append($removeFormA);

    $removeFormA.on('click', function(e) {
        // empêche le lien de créer un « # » dans l'URL
        e.preventDefault();

        // supprime l'élément tr pour le formulaire de offer
        $offerFormTr.remove();
    });
}