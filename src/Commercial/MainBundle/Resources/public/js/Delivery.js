var Delivery = function(parameters) {
    var $__s = this,
            $__params = parameters;
    var obj = {
        page: {
            /**
             * initializer
             * @returns {undefined}
             */
            _init: function() {
                obj.page._bindList();
                obj.page._bindDelete();
            },
            _bindList: function(){
              $(document).on('click', '.page-btn', function(e){
                  e.preventDefault();
                  if ($(this).attr('id') !== "") {
                    obj.list._showPage($(this).attr('id'));
                  }
              });
            },
            _bindDelete: function() {
                obj.row._delete($('.delete'));
            }
        },
        list: {
            _showPage: function(page) {
                obj.list._paginate(page);
            },
            _paginate: function(page) {
                $.ajax({
                    url: Routing.generate('commercial_main_delivery_list'),
                    type: 'post',
                    data: {'page': page},
                    success: function(response) {
                        $.unblockpage();
                        $('#list tbody').html(response.list);
                        $('.table-pagination').html(response.paginator);
                        obj.page._bindDelete();
                    },
                    error: function() {
                        bootbox.hideAll();
                        $.alert({
                            msg: 'Server error please try again later.',
                            callback: function() {
                                bootbox.hideAll();
                            }
                        });
                    },
                    beforSend: function() {
                        $.blockpage();
                    }
                });
            }
        },
        row: {
            _delete: function(self) {
                $('.delete').confirmation();
            }
        }
    };
    return {
        /**
         * intializer
         * @returns {undefined}
         */
        init: function() {
            obj.page._init();
        }
    };
};
Delivery.prototype = new EventDispatcher({}, 'delivery');