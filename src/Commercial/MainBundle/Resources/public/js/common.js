/**
 * Event Dispatcher class
 * @returns {EventDispatcher}
 */
var EventDispatcher = function(args, id) {
    this._events = {};
    this._id = id;
    if (id != 'undefined') {
        window[id] = this;
    }
};
/**
 * Event Dispatcher class constructor
 * @returns {undefined}
 */
EventDispatcher.prototype._construct = function() {
    for (i in this._events) {
        var ev = this._events[i];
        ev.initializer();
        $(ev.binder.selector).on(ev.binder.event, function() {
            $(document).trigger(ev.name);
        });
        $(document).on(ev.name, function() {
            eval(ev.fn);
        });
    }
};
/**
 * Event Dispatcher register class 
 * @returns {undefined}
 */
EventDispatcher.prototype.register = function(instance) {
    if (this._id) {
        window[this._id] = instance;
    }
};
/**
 * Event Dispatcher class getter
 * @returns {undefined}
 */
EventDispatcher.prototype.get = function(id) {
    if (window[id]) {
        return window[id];
    }
};

// ***********************
// ************* OVERRIDES
// ***********************

$.blockUI.defaults = {
    message: '<h1>Please wait...</h1>',
    title: null, // title string; only used when theme == true 
    draggable: true, // only used when theme == true (requires jquery-ui.js to be loaded) 
    theme: false, // set to true to use with jQuery UI themes 
    css: {
        padding: 0,
        margin: 0,
        width: '30%',
        top: '40%',
        left: '35%',
        textAlign: 'center',
        color: '#000',
        border: '3px solid #aaa',
        backgroundColor: '#fff',
        cursor: 'wait'
    },
    themedCSS: {
        width: '30%',
        top: '40%',
        left: '35%'
    },
    overlayCSS: {
        backgroundColor: '#FFF',
        opacity: 0.3,
        cursor: 'wait'
    },
    cursorReset: 'default',
    growlCSS: {
        width: '350px',
        top: '10px',
        left: '',
        right: '10px',
        border: 'none',
        padding: '5px',
        opacity: 0.6,
        cursor: null,
        color: '#fff',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px'
    },
    iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',
    forceIframe: false,
    baseZ: 1500,
    centerX: true, // <-- only effects element blocking (page block controlled via css above) 
    centerY: true,
    allowBodyStretch: true,
    bindEvents: true,
    constrainTabKey: true,
    fadeIn: 200,
    fadeOut: 400,
    timeout: 0,
    showOverlay: true,
    focusInput: true,
    onBlock: null,
    onUnblock: null,
    quirksmodeOffsetHack: 4,
    blockMsgClass: 'blockMsg',
    ignoreIfBlocked: false
};

// *************************
// ***************** GLOBALS
// *************************

(function($) {
    $.fn.toJSON = function(options) {
        options = $.extend({}, options);
        var self = this,
                json = {},
                push_counters = {},
                patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push": /^$/,
            "fixed": /^\d+$/,
            "named": /^[a-zA-Z0-9_]+$/
        };
        this.build = function(base, key, value) {
            base[key] = value;
            return base;
        };
        this.push_counter = function(key) {
            if (push_counters[key] === undefined) {
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };
        $.each($(this).serializeArray(), function() {
            if (!patterns.validate.test(this.name)) {
                return;
            }
            var k,
                    keys = this.name.match(patterns.key),
                    merge = this.value,
                    reverse_key = this.name;
            while ((k = keys.pop()) !== undefined) {
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
                if (k.match(patterns.push)) {
                    merge = self.build([], self.push_counter(reverse_key), merge);
                } else if (k.match(patterns.fixed)) {
                    merge = self.build([], k, merge);
                } else if (k.match(patterns.named)) {
                    merge = self.build({}, k, merge);
                }
            }
            json = $.extend(true, json, merge);
        });
        return json;
    };
    $.extend({
        /**
         * block the whole page
         * @param {type} options
         * @returns {undefined}
         */
        blockpage: function(options) {
            $.extend(defaults = {msg: null}, options);
            $(document).trigger('page.block', defaults.msg);
        },
        /**
         * unblock
         * @returns {undefined}
         */
        unblockpage: function() {
            $(document).trigger('page.unblock');
        },
        /**
         * active
         * niceScroller
         */
        niceScroller: function() {
            $(".modal-body").mCustomScrollbar();
        },
        /**
         * UI override of classic alert box
         * @param {type} options
         * @returns {undefined}
         */
        alert: function(options) {
            $.extend(defaults = {
                title: "Alert",
                msg: null,
                dialog: null,
                callback: null
            }, options);
            var callbackfn = function() {
                if(defaults.callback != null)
                {
                    defaults.callback();
                }
            }
            bootbox.alert(defaults.msg, callbackfn);
        },
        /**
         * UI override of classic confirm box
         * @param {type} options
         * @returns {undefined}
         */
        confirm: function(options) {
            $.extend(defaults = {
                title: "confirm",
                msg: null,
                ok: null,
                ko: null,
                dialog: null
            }, options);
            bootbox.confirm(defaults.msg, function(r) {
                if (r == true) {
                    if (defaults.ok != null) {
                        defaults.ok();
                    }
                } else {
                    if (defaults.ko != null) {
                        defaults.ko();
                    }
                }
            });
        },
        /**
         * * UI ajax dialog
         * @param {type} options
         * @returns {Boolean}
         */
        ajaxDialog: function(options) {
            var max_height = $(window).height() / 2;
            var scroller_opt = {
                advanced: {
                    updateOnContentResize: true,
                    autoScrollOnFocus: false
                }
            };
            var s = '.modal-body';
            var defaults = {};
            $.extend(defaults = {
                title: "UI dialog",
                path: null,
                success: null,
                innerSuccess: null,
                innerBeforeSend: null,
                innerFail: null,
                dialog: null
            }, options);
            var fnsuccess = function(data) {
                $.unblockpage();
                if (data == 1) {
                    if (defaults.innerSuccess != null) {
                        defaults.innerSuccess();
                    }
                    d.modal('hide');
                }
                else
                {
                    $(s).html(data);
                    $(s).mCustomScrollbar("destroy");
                    $(s).mCustomScrollbar(scroller_opt);
                    if (typeof(data) != 'undefined' && defaults.innerFail != null) {
                        defaults.innerFail();
                    }
                }
                var $frm = $('form', $(s));
                $frm.bind('submit', function() {
                    $.blockpage();
                    var post_data = $.extend($(this).serializeArray(), {});
                    $.ajax({
                        type: $frm.attr('method'),
                        url: $frm.attr('action'),
                        data: post_data,
                        success: fnsuccess,
                        beforeSend: fnbeforeSend
                    });
                    return false;
                });
            };
            var fnbeforeSend = function() {
                if (defaults.innerBeforeSend != null) {
                    defaults.innerBeforeSend();
                }
            };
            $(".bootbox .modal").ready(function() {
                $.blockpage();
                $.ajax({
                    url: defaults.path,
                    type: 'GET',
                    success: function(data) {
                        $.unblockpage();
                        $(s).html(data);
                        $(s).mCustomScrollbar(scroller_opt);
                        if (defaults.success != null) {
                            defaults.success();
                        }
                        fnsuccess();
                        $.unblockpage();
                    },
                    beforeSend: fnbeforeSend()
                });
            });
            var d = bootbox.dialog('Loading...', [{
                    "label": "Cancel",
                    "class": "btn"
                }, {
                    "label": "Save changes",
                    "class": "btn-primary",
                    "callback": function() {
                        $(s).find('form').submit();
                        return false;
                    }
                }
            ], {header: defaults.title});
            return false;
        }
    });
})(jQuery);



// ************************
// *************** TRIGGERS
// ************************

$(document).on('page.block', function(event, msg) {
    $('body').block({
        message: null
    });
    if (msg) {
        $('body').append('<div class="loader_message"> ' + msg + '... </div>');
    }
}).on('page.unblock', function() {
    $('body').unblock();
    $('.loader_message').remove();
});