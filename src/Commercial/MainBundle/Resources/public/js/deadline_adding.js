var collectionHolder = $('div.items');
var collectionHolder2 = $('div.items_2');

var $addItemLink = $('<a href="#" class="add_item_link">+ Ajouter un article</a>');
var $addItemLink2 = $('<a href="#" class="add_item_link">+ Ajouter une date</a>');
var $newLinkTr = $('<div class="span2" style="float: right;"></div>').append($addItemLink);
var $newLinkTr2 = $('<div class="span4" style="float: right;"></div>').append($addItemLink2);


$(document).ready(function() {
    // ajoute l'ancre « ajouter un offer » et tr à la balise ul
    collectionHolder.append($newLinkTr);
    collectionHolder2.append($newLinkTr2);
    
    $addItemLink.on('click', function(e) {
        // empêche le lien de créer un « # » dans l'URL
        e.preventDefault();

        // ajoute un nouveau formulaire offer (voir le prochain bloc de code)
        addItemForm(collectionHolder, $newLinkTr);
    });
    
    collectionHolder.find('.item').each(function() {
        addItemFormDeleteLink($(this));
    });
    
    $addItemLink2.on('click', function(e) {
        // empêche le lien de créer un « # » dans l'URL
        e.preventDefault();

        // ajoute un nouveau formulaire offer (voir le prochain bloc de code)
        addItemForm2(collectionHolder2, $newLinkTr2);
    });
    
    collectionHolder2.find('.item_2').each(function() {
        addItemFormDeleteLink2($(this));
    });
});
function addItemForm(collectionHolder, $newLinkTr) {
    // Récupère l'élément ayant l'attribut data-prototype comme expliqué plus tôt
    var prototype = collectionHolder.attr('data-prototype');

    // Remplace '__name__' dans le HTML du prototype par un nombre basé sur
    // la longueur de la collection courante
    var newForm = prototype.replace(/__name__/g, collectionHolder.children().length);

    // Affiche le formulaire dans la page dans un tr, avant le lien "ajouter un offer"
    var $newFormTr = $('<div class="item"></div>').append(newForm);
    $newLinkTr.before($newFormTr);
    addItemFormDeleteLink($newFormTr);
    $('.select2-me').select2();
    $('.grid-me .select2-me').on('change', function(){
        var id_article = $(this).val(),
            id = $(this).attr('id');
       $.ajax({
           url: Routing.generate('commercial_main_article_get_price'),
           type: 'post',
           data: {'id_article': id_article},
           success: function (response){
               $('#'+id).closest('.span3').next().html(response.price.toFixed(3));
               $('#'+id).closest('.span3').next().next().html(response.qt);
               $('#'+id).closest('.span3').next().next().next().next().next().html(response.tva+' %');
           }
       });
    });
    $('.qt').on('keyup', function(){
       var pu = $(this).parent().prev().prev().text(),
           qt = $(this).val(),
           taux = $(this).parent().next().next().html(),
           tva = parseFloat(taux.replace(" %", "."));
       $(this).parent().next().html((pu*qt).toFixed(3));
       $(this).parent().next().next().next().html(((parseFloat(pu)+((pu*tva)/100))*qt).toFixed(3));;
   });
    $('.show-it').on('click', function(){
       $('#deadline').removeClass('hidden');
    });
    $('#c7').on('click', function(){
       $('#deadline').addClass('hidden');
    });
}

function addItemFormDeleteLink($offerFormTr) {
    var $removeFormA = $('<div class="span1" style="text-align: center;"><a class="btn delete" title="Effacer"><i class="icon-remove"></i></a></div>');
    $offerFormTr.append($removeFormA);

    $removeFormA.on('click', function(e) {
        // empêche le lien de créer un « # » dans l'URL
        e.preventDefault();

        // supprime l'élément tr pour le formulaire de offer
        $offerFormTr.remove();
    });
}

function addItemForm2(collectionHolder, $newLinkTr) {
    // Récupère l'élément ayant l'attribut data-prototype comme expliqué plus tôt
    var prototype = collectionHolder.attr('data-prototype');

    // Remplace '__name__' dans le HTML du prototype par un nombre basé sur
    // la longueur de la collection courante
    var newForm = prototype.replace(/__name__/g, collectionHolder.children().length);

    // Affiche le formulaire dans la page dans un tr, avant le lien "ajouter un offer"
    var $newFormTr = $('<div class="item_2"></div>').append(newForm);
    $newLinkTr.before($newFormTr);
    addItemFormDeleteLink($newFormTr);
    $('.datepicker').datepicker();
}

function addItemFormDeleteLink2($offerFormTr) {
    var $removeFormA = $('<div class="span1" style="text-align: center;"><a class="btn delete" title="Effacer"><i class="icon-remove"></i></a></div>');
    $offerFormTr.append($removeFormA);

    $removeFormA.on('click', function(e) {
        // empêche le lien de créer un « # » dans l'URL
        e.preventDefault();

        // supprime l'élément tr pour le formulaire de offer
        $offerFormTr.remove();
    });
}