var Store = function(parameters) {
    var $__s = this,
        $__params = parameters,
        columns = [
          {id: "reference", name: "réference", field: "reference"},
          {id: "name", name: "Nom", field: "name"},
          {id: "category", name: "Categorie", field: "category"},
          {id: "in_qte", name: "Quantité entrée", field: "in_qte", cssClass: "align-right"},
          {id: "out_qte", name: "Quantité sortie", field: "out_qte", cssClass: "align-right"},
          {id: "qte", name: "Quantité en stock", field: "qte", cssClass: "align-right"},
          {id: "qte_black", name: "Quantité sans facture", field: "qte_black", cssClass: "align-right"},
          {id: "sell_price", name: "Prix de vente", field: "sell_price", cssClass: "align-right"}
        ],
        options = {
          enableCellNavigation: true,
          enableColumnReorder: false,
          forceFitColumns: true
        },
        $_searchString = "",
        $_grid,
        $_dataView;
    var obj = {
        page: {
            /**
             * initializer
             * @returns {undefined}
             */
            _init: function() {
                obj.page._bindList();
            },
            _bindList: function(){
              $(document).ready(function(){
                    obj.list._showPage();
              });
            }
        },
        list: {
            _showPage: function() {
                $.ajax({
                    url: Routing.generate('commercial_main_warehousearticle_list'),
                    type: 'get',
                    success: function(response) {
                        $.unblockpage();
                        function myFilter(item, args) {
                          if (args.searchString !== "" && item["name"].indexOf(args.searchString) === -1) {
                            return false;
                          }

                          return true;
                        }
                        $_dataView = new Slick.Data.DataView({ inlineFilters: true });
                        $_grid = new Slick.Grid("#myGrid", $_dataView, columns, options);
                        $_grid.setSelectionModel(new Slick.RowSelectionModel());
                        
                        $_dataView.onRowCountChanged.subscribe(function (e, args) {
                          $_grid.updateRowCount();
                          $_grid.render();
                        });

                        $_dataView.onRowsChanged.subscribe(function (e, args) {
                          $_grid.invalidateRows(args.rows);
                          $_grid.render();
                        });
                        
                        $("#search").keyup(function (e) {
                          Slick.GlobalEditorLock.cancelCurrentEdit();

                          // clear on Esc
                          if (e.which === 27) {
                            this.value = "";
                          }
                          $_searchString = this.value;
                          updateFilter();
                        });

                        function updateFilter() {
                          $_dataView.setFilterArgs({
                            searchString: $_searchString
                          });
                          $_dataView.refresh();
                        }
                        
                        $_dataView.beginUpdate();
                        $_dataView.setItems(response.list);
                        $_dataView.setFilterArgs({
                          searchString: $_searchString
                        });
                        $_dataView.setFilter(myFilter);
                        $_dataView.endUpdate();
                        
                        $_dataView.syncGridSelection($_grid, true);
                    },
                    error: function() {
                        bootbox.hideAll();
                        $.alert({
                            msg: 'Server error please try again later.',
                            callback: function() {
                                bootbox.hideAll();
                            }
                        });
                    },
                    beforSend: function() {
                        $.blockpage();
                    }
                });
                
            }
        }
    };
    return {
        /**
         * intializer
         * @returns {undefined}
         */
        init: function() {
            obj.page._init();
        }
    };
};
Store.prototype = new EventDispatcher({}, 'store');