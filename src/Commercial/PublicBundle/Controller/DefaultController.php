<?php

namespace Commercial\PublicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CommercialPublicBundle:Default:index.html.twig');
    }
}
