<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quote
 */
class Quote
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $num;

    /**
     * @var \DateTime
     */
    private $quote_date;

    /**
     * @var float
     */
    private $discount;

    /**
     * @var float
     */
    private $total;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $quote_articles;

    /**
     * @var \Commercial\CoreBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \Proxima\UserBundle\Entity\User
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->quote_articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param integer $num
     * @return Quote
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return integer 
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set quote_date
     *
     * @param \DateTime $quoteDate
     * @return Quote
     */
    public function setQuoteDate($quoteDate)
    {
        $this->quote_date = $quoteDate;

        return $this;
    }

    /**
     * Get quote_date
     *
     * @return \DateTime 
     */
    public function getQuoteDate()
    {
        return $this->quote_date;
    }

    /**
     * Set discount
     *
     * @param float $discount
     * @return Quote
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Quote
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Quote
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Quote
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add quote_articles
     *
     * @param \Commercial\CoreBundle\Entity\QuoteArticle $quoteArticles
     * @return Quote
     */
    public function addQuoteArticle(\Commercial\CoreBundle\Entity\QuoteArticle $quoteArticles)
    {
        $this->quote_articles[] = $quoteArticles;

        return $this;
    }

    /**
     * Remove quote_articles
     *
     * @param \Commercial\CoreBundle\Entity\QuoteArticle $quoteArticles
     */
    public function removeQuoteArticle(\Commercial\CoreBundle\Entity\QuoteArticle $quoteArticles)
    {
        $this->quote_articles->removeElement($quoteArticles);
    }

    /**
     * Get quote_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQuoteArticles()
    {
        return $this->quote_articles;
    }

    /**
     * Set customer
     *
     * @param \Commercial\CoreBundle\Entity\Customer $customer
     * @return Quote
     */
    public function setCustomer(\Commercial\CoreBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Commercial\CoreBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set user
     *
     * @param \Proxima\UserBundle\Entity\User $user
     * @return Quote
     */
    public function setUser(\Proxima\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Proxima\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
