<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoice
 */
class Invoice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $num;

    /**
     * @var \DateTime
     */
    private $invoice_date;

    /**
     * @var float
     */
    private $discount;

    /**
     * @var float
     */
    private $total;

    /**
     * @var integer
     */
    private $paid;

    /**
     * @var integer
     */
    private $payment_type;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $invoice_articles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $deadlines;

    /**
     * @var \Commercial\CoreBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \Proxima\UserBundle\Entity\User
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->invoice_articles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->deadlines = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param integer $num
     * @return Invoice
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return integer 
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set invoice_date
     *
     * @param \DateTime $invoiceDate
     * @return Invoice
     */
    public function setInvoiceDate($invoiceDate)
    {
        $this->invoice_date = $invoiceDate;

        return $this;
    }

    /**
     * Get invoice_date
     *
     * @return \DateTime 
     */
    public function getInvoiceDate()
    {
        return $this->invoice_date;
    }

    /**
     * Set discount
     *
     * @param float $discount
     * @return Invoice
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Invoice
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set paid
     *
     * @param integer $paid
     * @return Invoice
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return integer 
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set payment_type
     *
     * @param integer $paymentType
     * @return Invoice
     */
    public function setPaymentType($paymentType)
    {
        $this->payment_type = $paymentType;

        return $this;
    }

    /**
     * Get payment_type
     *
     * @return integer 
     */
    public function getPaymentType()
    {
        return $this->payment_type;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Invoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Invoice
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add invoice_articles
     *
     * @param \Commercial\CoreBundle\Entity\InvoiceArticle $invoiceArticles
     * @return Invoice
     */
    public function addInvoiceArticle(\Commercial\CoreBundle\Entity\InvoiceArticle $invoiceArticles)
    {
        $this->invoice_articles[] = $invoiceArticles;

        return $this;
    }

    /**
     * Remove invoice_articles
     *
     * @param \Commercial\CoreBundle\Entity\InvoiceArticle $invoiceArticles
     */
    public function removeInvoiceArticle(\Commercial\CoreBundle\Entity\InvoiceArticle $invoiceArticles)
    {
        $this->invoice_articles->removeElement($invoiceArticles);
    }

    /**
     * Get invoice_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvoiceArticles()
    {
        return $this->invoice_articles;
    }

    /**
     * Add deadlines
     *
     * @param \Commercial\CoreBundle\Entity\Deadline $deadlines
     * @return Invoice
     */
    public function addDeadline(\Commercial\CoreBundle\Entity\Deadline $deadlines)
    {
        $this->deadlines[] = $deadlines;

        return $this;
    }

    /**
     * Remove deadlines
     *
     * @param \Commercial\CoreBundle\Entity\Deadline $deadlines
     */
    public function removeDeadline(\Commercial\CoreBundle\Entity\Deadline $deadlines)
    {
        $this->deadlines->removeElement($deadlines);
    }

    /**
     * Get deadlines
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeadlines()
    {
        return $this->deadlines;
    }

    /**
     * Set customer
     *
     * @param \Commercial\CoreBundle\Entity\Customer $customer
     * @return Invoice
     */
    public function setCustomer(\Commercial\CoreBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Commercial\CoreBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set user
     *
     * @param \Proxima\UserBundle\Entity\User $user
     * @return Invoice
     */
    public function setUser(\Proxima\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Proxima\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
