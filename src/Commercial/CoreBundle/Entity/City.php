<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 */
class City
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $customers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $suppliers;

    /**
     * @var \Commercial\CoreBundle\Entity\Country
     */
    private $country;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->customers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->suppliers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add customers
     *
     * @param \Commercial\CoreBundle\Entity\Customer $customers
     * @return City
     */
    public function addCustomer(\Commercial\CoreBundle\Entity\Customer $customers)
    {
        $this->customers[] = $customers;

        return $this;
    }

    /**
     * Remove customers
     *
     * @param \Commercial\CoreBundle\Entity\Customer $customers
     */
    public function removeCustomer(\Commercial\CoreBundle\Entity\Customer $customers)
    {
        $this->customers->removeElement($customers);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Add suppliers
     *
     * @param \Commercial\CoreBundle\Entity\Supplier $suppliers
     * @return City
     */
    public function addSupplier(\Commercial\CoreBundle\Entity\Supplier $suppliers)
    {
        $this->suppliers[] = $suppliers;

        return $this;
    }

    /**
     * Remove suppliers
     *
     * @param \Commercial\CoreBundle\Entity\Supplier $suppliers
     */
    public function removeSupplier(\Commercial\CoreBundle\Entity\Supplier $suppliers)
    {
        $this->suppliers->removeElement($suppliers);
    }

    /**
     * Get suppliers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSuppliers()
    {
        return $this->suppliers;
    }

    /**
     * Set country
     *
     * @param \Commercial\CoreBundle\Entity\Country $country
     * @return City
     */
    public function setCountry(\Commercial\CoreBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Commercial\CoreBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
}
