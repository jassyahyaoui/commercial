<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DeliveryOrder
 */
class DeliveryOrder
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $num;

    /**
     * @var \DateTime
     */
    private $delivery_date;

    /**
     * @var float
     */
    private $discount;

    /**
     * @var float
     */
    private $total;

    /**
     * @var boolean
     */
    private $black;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $delivery_articles;

    /**
     * @var \Commercial\CoreBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \Proxima\UserBundle\Entity\User
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->delivery_articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param integer $num
     * @return DeliveryOrder
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return integer 
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set delivery_date
     *
     * @param \DateTime $deliveryDate
     * @return DeliveryOrder
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->delivery_date = $deliveryDate;

        return $this;
    }

    /**
     * Get delivery_date
     *
     * @return \DateTime 
     */
    public function getDeliveryDate()
    {
        return $this->delivery_date;
    }

    /**
     * Set discount
     *
     * @param float $discount
     * @return DeliveryOrder
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return DeliveryOrder
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set black
     *
     * @param boolean $black
     * @return DeliveryOrder
     */
    public function setBlack($black)
    {
        $this->black = $black;

        return $this;
    }

    /**
     * Get black
     *
     * @return boolean 
     */
    public function getBlack()
    {
        return $this->black;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return DeliveryOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return DeliveryOrder
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add delivery_articles
     *
     * @param \Commercial\CoreBundle\Entity\DeliveryArticle $deliveryArticles
     * @return DeliveryOrder
     */
    public function addDeliveryArticle(\Commercial\CoreBundle\Entity\DeliveryArticle $deliveryArticles)
    {
        $this->delivery_articles[] = $deliveryArticles;

        return $this;
    }

    /**
     * Remove delivery_articles
     *
     * @param \Commercial\CoreBundle\Entity\DeliveryArticle $deliveryArticles
     */
    public function removeDeliveryArticle(\Commercial\CoreBundle\Entity\DeliveryArticle $deliveryArticles)
    {
        $this->delivery_articles->removeElement($deliveryArticles);
    }

    /**
     * Get delivery_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeliveryArticles()
    {
        return $this->delivery_articles;
    }

    /**
     * Set customer
     *
     * @param \Commercial\CoreBundle\Entity\Customer $customer
     * @return DeliveryOrder
     */
    public function setCustomer(\Commercial\CoreBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Commercial\CoreBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set user
     *
     * @param \Proxima\UserBundle\Entity\User $user
     * @return DeliveryOrder
     */
    public function setUser(\Proxima\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Proxima\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
