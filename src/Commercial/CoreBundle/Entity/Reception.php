<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reception
 */
class Reception
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $num;

    /**
     * @var boolean
     */
    private $black;

    /**
     * @var float
     */
    private $total;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $reception_articles;

    /**
     * @var \Commercial\CoreBundle\Entity\Warehouse
     */
    private $warehouse;

    /**
     * @var \Commercial\CoreBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \Proxima\UserBundle\Entity\User
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reception_articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param integer $num
     * @return Reception
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return integer 
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set black
     *
     * @param boolean $black
     * @return Reception
     */
    public function setBlack($black)
    {
        $this->black = $black;

        return $this;
    }

    /**
     * Get black
     *
     * @return boolean 
     */
    public function getBlack()
    {
        return $this->black;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Reception
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Reception
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Reception
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add reception_articles
     *
     * @param \Commercial\CoreBundle\Entity\ReceptionArticle $receptionArticles
     * @return Reception
     */
    public function addReceptionArticle(\Commercial\CoreBundle\Entity\ReceptionArticle $receptionArticles)
    {
        $this->reception_articles[] = $receptionArticles;

        return $this;
    }

    /**
     * Remove reception_articles
     *
     * @param \Commercial\CoreBundle\Entity\ReceptionArticle $receptionArticles
     */
    public function removeReceptionArticle(\Commercial\CoreBundle\Entity\ReceptionArticle $receptionArticles)
    {
        $this->reception_articles->removeElement($receptionArticles);
    }

    /**
     * Get reception_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReceptionArticles()
    {
        return $this->reception_articles;
    }

    /**
     * Set warehouse
     *
     * @param \Commercial\CoreBundle\Entity\Warehouse $warehouse
     * @return Reception
     */
    public function setWarehouse(\Commercial\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \Commercial\CoreBundle\Entity\Warehouse 
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set supplier
     *
     * @param \Commercial\CoreBundle\Entity\Supplier $supplier
     * @return Reception
     */
    public function setSupplier(\Commercial\CoreBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \Commercial\CoreBundle\Entity\Supplier 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set user
     *
     * @param \Proxima\UserBundle\Entity\User $user
     * @return Reception
     */
    public function setUser(\Proxima\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Proxima\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
