<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 */
class Article
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $marge;

    /**
     * @var float
     */
    private $buy_price;

    /**
     * @var float
     */
    private $sell_price;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $warehouse_articles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $invoice_articles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $quote_articles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $delivery_articles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $reception_articles;

    /**
     * @var \Commercial\CoreBundle\Entity\Category
     */
    private $category;

    /**
     * @var \Commercial\CoreBundle\Entity\Tva
     */
    private $tva;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->warehouse_articles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->invoice_articles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->quote_articles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->delivery_articles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reception_articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Article
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Article
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set marge
     *
     * @param float $marge
     * @return Article
     */
    public function setMarge($marge)
    {
        $this->marge = $marge;

        return $this;
    }

    /**
     * Get marge
     *
     * @return float 
     */
    public function getMarge()
    {
        return $this->marge;
    }

    /**
     * Set buy_price
     *
     * @param float $buyPrice
     * @return Article
     */
    public function setBuyPrice($buyPrice)
    {
        $this->buy_price = $buyPrice;

        return $this;
    }

    /**
     * Get buy_price
     *
     * @return float 
     */
    public function getBuyPrice()
    {
        return $this->buy_price;
    }

    /**
     * Set sell_price
     *
     * @param float $sellPrice
     * @return Article
     */
    public function setSellPrice($sellPrice)
    {
        $this->sell_price = $sellPrice;

        return $this;
    }

    /**
     * Get sell_price
     *
     * @return float 
     */
    public function getSellPrice()
    {
        return $this->sell_price;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Article
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Article
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add warehouse_articles
     *
     * @param \Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles
     * @return Article
     */
    public function addWarehouseArticle(\Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles)
    {
        $this->warehouse_articles[] = $warehouseArticles;

        return $this;
    }

    /**
     * Remove warehouse_articles
     *
     * @param \Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles
     */
    public function removeWarehouseArticle(\Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles)
    {
        $this->warehouse_articles->removeElement($warehouseArticles);
    }

    /**
     * Get warehouse_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWarehouseArticles()
    {
        return $this->warehouse_articles;
    }

    /**
     * Add invoice_articles
     *
     * @param \Commercial\CoreBundle\Entity\InvoiceArticle $invoiceArticles
     * @return Article
     */
    public function addInvoiceArticle(\Commercial\CoreBundle\Entity\InvoiceArticle $invoiceArticles)
    {
        $this->invoice_articles[] = $invoiceArticles;

        return $this;
    }

    /**
     * Remove invoice_articles
     *
     * @param \Commercial\CoreBundle\Entity\InvoiceArticle $invoiceArticles
     */
    public function removeInvoiceArticle(\Commercial\CoreBundle\Entity\InvoiceArticle $invoiceArticles)
    {
        $this->invoice_articles->removeElement($invoiceArticles);
    }

    /**
     * Get invoice_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvoiceArticles()
    {
        return $this->invoice_articles;
    }

    /**
     * Add quote_articles
     *
     * @param \Commercial\CoreBundle\Entity\QuoteArticle $quoteArticles
     * @return Article
     */
    public function addQuoteArticle(\Commercial\CoreBundle\Entity\QuoteArticle $quoteArticles)
    {
        $this->quote_articles[] = $quoteArticles;

        return $this;
    }

    /**
     * Remove quote_articles
     *
     * @param \Commercial\CoreBundle\Entity\QuoteArticle $quoteArticles
     */
    public function removeQuoteArticle(\Commercial\CoreBundle\Entity\QuoteArticle $quoteArticles)
    {
        $this->quote_articles->removeElement($quoteArticles);
    }

    /**
     * Get quote_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQuoteArticles()
    {
        return $this->quote_articles;
    }

    /**
     * Add delivery_articles
     *
     * @param \Commercial\CoreBundle\Entity\DeliveryArticle $deliveryArticles
     * @return Article
     */
    public function addDeliveryArticle(\Commercial\CoreBundle\Entity\DeliveryArticle $deliveryArticles)
    {
        $this->delivery_articles[] = $deliveryArticles;

        return $this;
    }

    /**
     * Remove delivery_articles
     *
     * @param \Commercial\CoreBundle\Entity\DeliveryArticle $deliveryArticles
     */
    public function removeDeliveryArticle(\Commercial\CoreBundle\Entity\DeliveryArticle $deliveryArticles)
    {
        $this->delivery_articles->removeElement($deliveryArticles);
    }

    /**
     * Get delivery_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeliveryArticles()
    {
        return $this->delivery_articles;
    }

    /**
     * Add reception_articles
     *
     * @param \Commercial\CoreBundle\Entity\ReceptionArticle $receptionArticles
     * @return Article
     */
    public function addReceptionArticle(\Commercial\CoreBundle\Entity\ReceptionArticle $receptionArticles)
    {
        $this->reception_articles[] = $receptionArticles;

        return $this;
    }

    /**
     * Remove reception_articles
     *
     * @param \Commercial\CoreBundle\Entity\ReceptionArticle $receptionArticles
     */
    public function removeReceptionArticle(\Commercial\CoreBundle\Entity\ReceptionArticle $receptionArticles)
    {
        $this->reception_articles->removeElement($receptionArticles);
    }

    /**
     * Get reception_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReceptionArticles()
    {
        return $this->reception_articles;
    }

    /**
     * Set category
     *
     * @param \Commercial\CoreBundle\Entity\Category $category
     * @return Article
     */
    public function setCategory(\Commercial\CoreBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Commercial\CoreBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set tva
     *
     * @param \Commercial\CoreBundle\Entity\Tva $tva
     * @return Article
     */
    public function setTva(\Commercial\CoreBundle\Entity\Tva $tva = null)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva
     *
     * @return \Commercial\CoreBundle\Entity\Tva 
     */
    public function getTva()
    {
        return $this->tva;
    }
}
