<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WarehouseEntry
 */
class WarehouseEntry
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var boolean
     */
    private $black;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Commercial\CoreBundle\Entity\WarehouseArticle
     */
    private $warehouse_article;

    /**
     * @var \Proxima\UserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return WarehouseEntry
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set black
     *
     * @param boolean $black
     * @return WarehouseEntry
     */
    public function setBlack($black)
    {
        $this->black = $black;

        return $this;
    }

    /**
     * Get black
     *
     * @return boolean 
     */
    public function getBlack()
    {
        return $this->black;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return WarehouseEntry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return WarehouseEntry
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return WarehouseEntry
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set warehouse_article
     *
     * @param \Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticle
     * @return WarehouseEntry
     */
    public function setWarehouseArticle(\Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticle = null)
    {
        $this->warehouse_article = $warehouseArticle;

        return $this;
    }

    /**
     * Get warehouse_article
     *
     * @return \Commercial\CoreBundle\Entity\WarehouseArticle 
     */
    public function getWarehouseArticle()
    {
        return $this->warehouse_article;
    }

    /**
     * Set user
     *
     * @param \Proxima\UserBundle\Entity\User $user
     * @return WarehouseEntry
     */
    public function setUser(\Proxima\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Proxima\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
