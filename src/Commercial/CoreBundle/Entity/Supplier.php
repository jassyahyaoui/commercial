<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplier
 */
class Supplier
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $company_name;

    /**
     * @var string
     */
    private $tel;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $code_tva;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $warehouse_articles;

    /**
     * @var \Commercial\CoreBundle\Entity\City
     */
    private $city;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->warehouse_articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Supplier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Supplier
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set company_name
     *
     * @param string $companyName
     * @return Supplier
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;

        return $this;
    }

    /**
     * Get company_name
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Supplier
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return Supplier
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Supplier
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set code_tva
     *
     * @param string $codeTva
     * @return Supplier
     */
    public function setCodeTva($codeTva)
    {
        $this->code_tva = $codeTva;

        return $this;
    }

    /**
     * Get code_tva
     *
     * @return string 
     */
    public function getCodeTva()
    {
        return $this->code_tva;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Supplier
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Supplier
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Supplier
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add warehouse_articles
     *
     * @param \Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles
     * @return Supplier
     */
    public function addWarehouseArticle(\Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles)
    {
        $this->warehouse_articles[] = $warehouseArticles;

        return $this;
    }

    /**
     * Remove warehouse_articles
     *
     * @param \Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles
     */
    public function removeWarehouseArticle(\Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles)
    {
        $this->warehouse_articles->removeElement($warehouseArticles);
    }

    /**
     * Get warehouse_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWarehouseArticles()
    {
        return $this->warehouse_articles;
    }

    /**
     * Set city
     *
     * @param \Commercial\CoreBundle\Entity\City $city
     * @return Supplier
     */
    public function setCity(\Commercial\CoreBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Commercial\CoreBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }
}
