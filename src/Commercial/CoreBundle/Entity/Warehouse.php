<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Warehouse
 */
class Warehouse
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $tel;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $warehouse_articles;

    /**
     * @var \Commercial\CoreBundle\Entity\Store
     */
    private $store;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->warehouse_articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Warehouse
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Warehouse
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Warehouse
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Warehouse
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Warehouse
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Warehouse
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add warehouse_articles
     *
     * @param \Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles
     * @return Warehouse
     */
    public function addWarehouseArticle(\Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles)
    {
        $this->warehouse_articles[] = $warehouseArticles;

        return $this;
    }

    /**
     * Remove warehouse_articles
     *
     * @param \Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles
     */
    public function removeWarehouseArticle(\Commercial\CoreBundle\Entity\WarehouseArticle $warehouseArticles)
    {
        $this->warehouse_articles->removeElement($warehouseArticles);
    }

    /**
     * Get warehouse_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWarehouseArticles()
    {
        return $this->warehouse_articles;
    }

    /**
     * Set store
     *
     * @param \Commercial\CoreBundle\Entity\Store $store
     * @return Warehouse
     */
    public function setStore(\Commercial\CoreBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Commercial\CoreBundle\Entity\Store 
     */
    public function getStore()
    {
        return $this->store;
    }
}
