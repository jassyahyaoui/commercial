<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Deadline
 */
class Deadline
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $deadline_date;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Commercial\CoreBundle\Entity\Invoice
     */
    private $invoice;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deadline_date
     *
     * @param \DateTime $deadlineDate
     * @return Deadline
     */
    public function setDeadlineDate($deadlineDate)
    {
        $this->deadline_date = $deadlineDate;

        return $this;
    }

    /**
     * Get deadline_date
     *
     * @return \DateTime 
     */
    public function getDeadlineDate()
    {
        return $this->deadline_date;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Deadline
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Deadline
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set invoice
     *
     * @param \Commercial\CoreBundle\Entity\Invoice $invoice
     * @return Deadline
     */
    public function setInvoice(\Commercial\CoreBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \Commercial\CoreBundle\Entity\Invoice 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
}
