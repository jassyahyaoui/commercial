<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReceptionArticle
 */
class ReceptionArticle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Commercial\CoreBundle\Entity\Reception
     */
    private $reception;

    /**
     * @var \Commercial\CoreBundle\Entity\Article
     */
    private $article;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return ReceptionArticle
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return ReceptionArticle
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return ReceptionArticle
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set reception
     *
     * @param \Commercial\CoreBundle\Entity\Reception $reception
     * @return ReceptionArticle
     */
    public function setReception(\Commercial\CoreBundle\Entity\Reception $reception = null)
    {
        $this->reception = $reception;

        return $this;
    }

    /**
     * Get reception
     *
     * @return \Commercial\CoreBundle\Entity\Reception 
     */
    public function getReception()
    {
        return $this->reception;
    }

    /**
     * Set article
     *
     * @param \Commercial\CoreBundle\Entity\Article $article
     * @return ReceptionArticle
     */
    public function setArticle(\Commercial\CoreBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Commercial\CoreBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}
