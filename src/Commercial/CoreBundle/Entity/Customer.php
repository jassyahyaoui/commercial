<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 */
class Customer
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $company_name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $tel;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $email;

    /**
     * @var integer
     */
    private $tva_type;

    /**
     * @var string
     */
    private $code_tva;

    /**
     * @var string
     */
    private $bank_name;

    /**
     * @var string
     */
    private $bank_rib;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $invoices;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $quotes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $deliveries;

    /**
     * @var \Commercial\CoreBundle\Entity\City
     */
    private $city;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->invoices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->quotes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->deliveries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Customer
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set company_name
     *
     * @param string $companyName
     * @return Customer
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;

        return $this;
    }

    /**
     * Get company_name
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Customer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Customer
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return Customer
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set tva_type
     *
     * @param integer $tvaType
     * @return Customer
     */
    public function setTvaType($tvaType)
    {
        $this->tva_type = $tvaType;

        return $this;
    }

    /**
     * Get tva_type
     *
     * @return integer 
     */
    public function getTvaType()
    {
        return $this->tva_type;
    }

    /**
     * Set code_tva
     *
     * @param string $codeTva
     * @return Customer
     */
    public function setCodeTva($codeTva)
    {
        $this->code_tva = $codeTva;

        return $this;
    }

    /**
     * Get code_tva
     *
     * @return string 
     */
    public function getCodeTva()
    {
        return $this->code_tva;
    }

    /**
     * Set bank_name
     *
     * @param string $bankName
     * @return Customer
     */
    public function setBankName($bankName)
    {
        $this->bank_name = $bankName;

        return $this;
    }

    /**
     * Get bank_name
     *
     * @return string 
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * Set bank_rib
     *
     * @param string $bankRib
     * @return Customer
     */
    public function setBankRib($bankRib)
    {
        $this->bank_rib = $bankRib;

        return $this;
    }

    /**
     * Get bank_rib
     *
     * @return string 
     */
    public function getBankRib()
    {
        return $this->bank_rib;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Customer
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Customer
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add invoices
     *
     * @param \Commercial\CoreBundle\Entity\Invoice $invoices
     * @return Customer
     */
    public function addInvoice(\Commercial\CoreBundle\Entity\Invoice $invoices)
    {
        $this->invoices[] = $invoices;

        return $this;
    }

    /**
     * Remove invoices
     *
     * @param \Commercial\CoreBundle\Entity\Invoice $invoices
     */
    public function removeInvoice(\Commercial\CoreBundle\Entity\Invoice $invoices)
    {
        $this->invoices->removeElement($invoices);
    }

    /**
     * Get invoices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * Add quotes
     *
     * @param \Commercial\CoreBundle\Entity\Quote $quotes
     * @return Customer
     */
    public function addQuote(\Commercial\CoreBundle\Entity\Quote $quotes)
    {
        $this->quotes[] = $quotes;

        return $this;
    }

    /**
     * Remove quotes
     *
     * @param \Commercial\CoreBundle\Entity\Quote $quotes
     */
    public function removeQuote(\Commercial\CoreBundle\Entity\Quote $quotes)
    {
        $this->quotes->removeElement($quotes);
    }

    /**
     * Get quotes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQuotes()
    {
        return $this->quotes;
    }

    /**
     * Add deliveries
     *
     * @param \Commercial\CoreBundle\Entity\DeliveryOrder $deliveries
     * @return Customer
     */
    public function addDelivery(\Commercial\CoreBundle\Entity\DeliveryOrder $deliveries)
    {
        $this->deliveries[] = $deliveries;

        return $this;
    }

    /**
     * Remove deliveries
     *
     * @param \Commercial\CoreBundle\Entity\DeliveryOrder $deliveries
     */
    public function removeDelivery(\Commercial\CoreBundle\Entity\DeliveryOrder $deliveries)
    {
        $this->deliveries->removeElement($deliveries);
    }

    /**
     * Get deliveries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeliveries()
    {
        return $this->deliveries;
    }

    /**
     * Set city
     *
     * @param \Commercial\CoreBundle\Entity\City $city
     * @return Customer
     */
    public function setCity(\Commercial\CoreBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Commercial\CoreBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }
}
