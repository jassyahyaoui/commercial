<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DeliveryArticle
 */
class DeliveryArticle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $black;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Commercial\CoreBundle\Entity\DeliveryOrder
     */
    private $delivery;

    /**
     * @var \Commercial\CoreBundle\Entity\Article
     */
    private $article;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return DeliveryArticle
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return DeliveryArticle
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set black
     *
     * @param boolean $black
     * @return DeliveryArticle
     */
    public function setBlack($black)
    {
        $this->black = $black;

        return $this;
    }

    /**
     * Get black
     *
     * @return boolean 
     */
    public function getBlack()
    {
        return $this->black;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return DeliveryArticle
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return DeliveryArticle
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set delivery
     *
     * @param \Commercial\CoreBundle\Entity\DeliveryOrder $delivery
     * @return DeliveryArticle
     */
    public function setDelivery(\Commercial\CoreBundle\Entity\DeliveryOrder $delivery = null)
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * Get delivery
     *
     * @return \Commercial\CoreBundle\Entity\DeliveryOrder 
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Set article
     *
     * @param \Commercial\CoreBundle\Entity\Article $article
     * @return DeliveryArticle
     */
    public function setArticle(\Commercial\CoreBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Commercial\CoreBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}
