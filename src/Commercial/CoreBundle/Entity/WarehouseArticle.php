<?php

namespace Commercial\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WarehouseArticle
 */
class WarehouseArticle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var integer
     */
    private $quantity_black;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $warehouse_entries;

    /**
     * @var \Commercial\CoreBundle\Entity\Warehouse
     */
    private $warehouse;

    /**
     * @var \Commercial\CoreBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \Commercial\CoreBundle\Entity\Article
     */
    private $article;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->warehouse_entries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return WarehouseArticle
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set quantity_black
     *
     * @param integer $quantityBlack
     * @return WarehouseArticle
     */
    public function setQuantityBlack($quantityBlack)
    {
        $this->quantity_black = $quantityBlack;

        return $this;
    }

    /**
     * Get quantity_black
     *
     * @return integer 
     */
    public function getQuantityBlack()
    {
        return $this->quantity_black;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return WarehouseArticle
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return WarehouseArticle
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add warehouse_entries
     *
     * @param \Commercial\CoreBundle\Entity\WarehouseEntry $warehouseEntries
     * @return WarehouseArticle
     */
    public function addWarehouseEntry(\Commercial\CoreBundle\Entity\WarehouseEntry $warehouseEntries)
    {
        $this->warehouse_entries[] = $warehouseEntries;

        return $this;
    }

    /**
     * Remove warehouse_entries
     *
     * @param \Commercial\CoreBundle\Entity\WarehouseEntry $warehouseEntries
     */
    public function removeWarehouseEntry(\Commercial\CoreBundle\Entity\WarehouseEntry $warehouseEntries)
    {
        $this->warehouse_entries->removeElement($warehouseEntries);
    }

    /**
     * Get warehouse_entries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWarehouseEntries()
    {
        return $this->warehouse_entries;
    }

    /**
     * Set warehouse
     *
     * @param \Commercial\CoreBundle\Entity\Warehouse $warehouse
     * @return WarehouseArticle
     */
    public function setWarehouse(\Commercial\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \Commercial\CoreBundle\Entity\Warehouse 
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set supplier
     *
     * @param \Commercial\CoreBundle\Entity\Supplier $supplier
     * @return WarehouseArticle
     */
    public function setSupplier(\Commercial\CoreBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \Commercial\CoreBundle\Entity\Supplier 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set article
     *
     * @param \Commercial\CoreBundle\Entity\Article $article
     * @return WarehouseArticle
     */
    public function setArticle(\Commercial\CoreBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Commercial\CoreBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}
