<?php

namespace Commercial\CoreBundle\Common\Util\ProximaTable;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class ProximaTableManipulator
{
    
    /**
     *
     * @var EntityManager 
     */
    protected $_em;
    /**
     *
     * @var Container
     */
    protected $_container;

    /**
     * Constructor
     * 
     * @param \Doctrine\ORM\EntityManager $EntityManager
     */ 
    public function __construct(EntityManager $EntityManager, Container $container)
    {
        $this->_em = $EntityManager;
        $this->_container = $container;
    }
    /**
     * Generate ajax calls table liste
     * 
     * @param string $name_space
     * @param string $bundle
     * @param string $class
     * @param int $lastPage
     * @param int $current_page
     * @param array $entities
     * @param array $params
     *      [
     *          ['field' => 'id', 'label' => 'ID'], (required)
     *          ['field' => 'name', 'label' => 'Name'],
     *          ['field' => 'description', 'label' => 'Description']
     *      ];
     * @param string $css_class
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function generateTable($name_space, $bundle, $class, $lastPage, $current_page, $entities, array $params = null, $css_class='')
    {
        $count_params = count($params);
        if($count_params)
        {
            $paginator = $this->_container->get('templating')->render('CommercialCoreBundle:ProximaTable:common/_paginator.html.twig',array(
                'lastpage' => $lastPage,
                'page'     => $current_page
            ));
            $list = $this->_container->get('templating')->render('CommercialCoreBundle:ProximaTable:common/_list.html.twig',array(
                'fields'     => $params,
                'list'       => $entities,
                'name_space' => strtolower($name_space),
                'bundle'     => strtolower(str_replace('Bundle','', $bundle)),
                'class'      => strtolower($class),
            ));
            return $this->_container->get('templating')->render('CommercialCoreBundle:ProximaTable:_proxima_table.html.twig',array(
                'table_header'=> $params,
                'list'     =>$list,
                'lastpage' => $lastPage,
                'page'     => $current_page,
                'paginator'=> $paginator,
                'css_class'=> $css_class
            ));
        }
        return null;
    }

    public function paginateList($name_space, $bundle, $class, $lastPage, $current_page, $entities, array $params = null)
    {
        $count_params = count($params);
        if($count_params)
        {

        $paginator = $this->_container->get('templating')->render('CommercialCoreBundle:ProximaTable:common/_paginator.html.twig',array(
            'lastpage' => $lastPage,
            'page'     => $current_page
        ));
        $list = $this->_container->get('templating')->render('CommercialCoreBundle:ProximaTable:common/_list.html.twig',array(
            'fields'     => $params,
            'list'       => $entities,
            'name_space' => strtolower($name_space),
            'bundle'     => strtolower(str_replace('Bundle','', $bundle)),
            'class'      => strtolower($class),
        ));

        return ['list' => $list, 'paginator' => $paginator ];

        }
        return null;
    }
}
