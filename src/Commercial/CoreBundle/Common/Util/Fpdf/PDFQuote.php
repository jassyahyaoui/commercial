<?php
namespace Commercial\CoreBundle\Common\Util\Fpdf;
use Commercial\CoreBundle\Common\Util\Fpdf\FPDF;
use Commercial\CoreBundle\Common\Util\Fpdf\FPDF_CellFit;

class PDFQuote extends FPDF
{

// Chargement des données
 function __construct()
 {
     parent::__construct();
 }
 
 function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-18);
    
    // Police Arial italique 8
    $this->SetFont('Arial','',10);
    // Numéro de page
    $this->Cell(0,4,'_________________________________________________________________________________________________________','',0,'C');
    $this->Ln();
    $this->Cell(0,6,iconv('UTF-8', 'windows-1252','Siége: 23 rue el Quods cité ettadhamen-Ariana Tél: 31154964-98620872-22552168-54076416'),'',0,'C');
     $this->Ln();
    $this->Cell(0,6,'MF: 000MN1168824/R RC: B3181992010 ','',0,'C');

            
   
}

function Header()
{
    
    //Arial bold 15
    $this->SetFont('Arial','B',15);
    //Move to the right
    

    //Title
   // $this->Image('bundles/commercialmain/images/bl.png',10,9,50);
    $this->Cell(20,6,'Devis',0,0,'R');
    //Line break
    $this->Ln(4);
    $this->SetFont('Arial','',10);
    $this->Cell(0,4,'_________________________________________________________________________________________________________','',0,'C');
     
   
    $this->Ln(6);
    
    
}



// Tableau simple
function BasicTable($header, $data_entete, $sub,$url,$data, $som, $dis, $tva)
{      
         
    
          $this->Image($url,150,25,50);
         $this->SetFont('Arial','',8);
         $this->Cell(198,65,iconv('UTF-8', 'windows-1252','Vente et distribution des boissons gazeuses '
 . ' & eaux minérales'),'',0,'R');
             $this->SetFont('','',12);
      
        foreach($data_entete as $row)
        {
            $this->Cell(35,6,iconv('UTF-8','windows-1252',$row),'',3);
            $this->Ln();
           
        }
     
                
                $this->Ln();
                $this->Cell(182, 6, $sub[0],'',0,'R');
                $this->Ln();
                $this->Cell(180, 8, $sub[1],'',0,'R');
                $this->Ln();     
        
        
                 // Largeurs des colonnes
    $w = array(100, 25, 25, 45);
    // En-tête
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C');
    $this->Ln();

   $sout=128;
     foreach($data as $row)
    {
        $this->CellFitScale($w[0],8,iconv('UTF-8', 'windows-1252',$row[1]),1,0,'L');
         $this->Cell($w[1],8,$row[2],1,0,'C');
          $this->Cell($w[2],8,$row[3],1,0,'C');
           $this->Cell($w[3],8,$row[4],1,0,'R');
        $sout-=8;
 
        $this->Ln();
    }
    $a_payer=$som-$dis+$tva;
 
    $this->Cell($w[0],$sout,'',1,0);
    $this->Cell($w[1],$sout,'',1,0);
    $this->Cell($w[2],$sout,'',1,0);
    $this->Cell($w[3],$sout,'',1,0);
  
    
    $this->Ln();
    $w = array(100, 25, 25,10,35);
   
    
    $this->Cell($w[0],10,'',0,0,'C');
    $this->Cell($w[1],10,'',0,0,'C');
    $this->Cell($w[2],10,'TOTAL',0,0,'C');
    $this->Cell($w[3],10,'DT',0,0,'C');
    $this->Cell($w[4],10,number_format($som, 3, '.', ' '),0,0,'R');
    
    
      $this->Ln();
    $this->Cell($w[0],10,'',0,0,'C');
    $this->Cell($w[1],10,'',0,0,'C');
    $this->Cell($w[2],10,'TVA',0,0,'C');
    $this->Cell($w[3],10,'DT',0,0,'C');
    $this->Cell($w[4],10,number_format($tva, 3, '.', ' '),0,0,'R');
    
    
      $this->Ln();
    $this->Cell($w[0],10,'',0,0,'C');
    $this->Cell($w[1],10,'',0,0,'C'); 
    $this->Cell($w[2],10,'REMISE',0,0,'C');
    $this->Cell($w[3],10,'DT',0,0,'C');
    $this->Cell($w[4],10,number_format($dis, 3, '.', ' '),0,0,'R');
    
   
      $this->Ln();
    $this->Cell($w[0],10,'',0,0,'C');
    $this->Cell($w[1],10,'MONTANT',0,0,'R'); 
    $this->Cell($w[2],10,'A PAYEE',0,0,'C');
    $this->Cell($w[3],10,'DT',0,0,'C');
    $this->Cell($w[4],10,number_format($a_payer, 3, '.', ' '),0,0,'R');
    
   
     $this->Ln(10);
   $this->SetXY(160,223);
   $this->drawTextBox("", 45, 40, 'C', 'M');
   
   
}
}

?>