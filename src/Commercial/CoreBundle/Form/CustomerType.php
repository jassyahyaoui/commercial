<?php

namespace Commercial\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('company_name')
            ->add('description')
            ->add('tel')
            ->add('mobile')
            ->add('tva_type')
            ->add('code_tva')
            ->add('bank_name')
            ->add('bank_rib')
            ->add('city', 'entity', array('class'      => 'CommercialCoreBundle:City',
                                             'required'   => FALSE,
                                             'property'      =>'name',
                                             'empty_value'=> ' Choisissez une ville'))
            ->add('email')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Commercial\CoreBundle\Entity\Customer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commercial_corebundle_customer';
    }
}
