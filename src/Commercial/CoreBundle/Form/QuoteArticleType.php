<?php

namespace Commercial\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class QuoteArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity')
            ->add('description')
            ->add('article', 'entity', array(
                'label'         =>'Article:',
                'empty_value'   =>'Choisissez un article',
                'class'         =>'CommercialCoreBundle:Article',
                'property'      =>'name'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Commercial\CoreBundle\Entity\QuoteArticle'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commercial_corebundle_quotearticle';
    }
}
