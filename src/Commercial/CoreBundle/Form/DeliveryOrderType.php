<?php

namespace Commercial\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DeliveryOrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('delivery_date', null, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'data' => new \DateTime()
	        ))
            ->add('black', 'checkbox',[
                'label'=> 'sans facture',
                'required'=> false
            ])
            ->add('num')
            ->add('discount')
            ->add('total')
            ->add('customer', 'entity', array(
                'label'         =>'Client:',
                'empty_value'   =>'Choisissez un client',
                'class'         =>'CommercialCoreBundle:Customer',
                'property'      =>'company_name'
            ))
            ->add('delivery_articles', 'collection', array(
                'type' => new DeliveryArticleType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Commercial\CoreBundle\Entity\DeliveryOrder'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commercial_corebundle_deliveryorder';
    }
}
