<?php

namespace Commercial\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WarehouseArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity')
            ->add('warehouse', 'entity', array(
                'label'         =>'Dépôt:',
                'empty_value'   =>'Choisissez un dépôt',
                'class'         =>'CommercialCoreBundle:Warehouse',
                'property'      =>'name',
                'required'      =>false
            ))
            ->add('article', 'entity', array(
                'label'         =>'Article:',
                'empty_value'   =>'Choisissez un article',
                'class'         =>'CommercialCoreBundle:Article',
                'property'      =>'name',
                'required'      =>false
            ))
            ->add('supplier', 'entity', array(
                'label'         =>'Fournisseur:',
                'empty_value'   =>'Choisissez un fournisseur',
                'class'         =>'CommercialCoreBundle:Supplier',
                'property'      =>'name',
                'required'      =>false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Commercial\CoreBundle\Entity\WarehouseArticle'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commercial_corebundle_warehousearticle';
    }
}
