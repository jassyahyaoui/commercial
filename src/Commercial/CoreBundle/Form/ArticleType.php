<?php

namespace Commercial\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityRepository;

class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference')
            ->add('name')
            ->add('buy_price')
            ->add('marge')
            ->add('sell_price')
            ->add('category', 'entity', array(
                'label'         =>'Famille:',
                'empty_value'   =>'Choisissez une famille',
                'class'         =>'CommercialCoreBundle:Category',
                'query_builder' => function(EntityRepository $er) {
                return $er->getLeafsQueryBuilder($er->getRootNodes()[0]);
                                },
                'property'      =>'name'
            ))
            ->add('tva', 'entity',array(
                'label'         =>'tva:',
                'empty_value'   =>'Choisissez un taux',
                'class'         =>'CommercialCoreBundle:Tva',
                'property'      =>'name'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Commercial\CoreBundle\Entity\Article'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commercial_corebundle_article';
    }
}
