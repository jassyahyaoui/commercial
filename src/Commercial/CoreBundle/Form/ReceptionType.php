<?php

namespace Commercial\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Commercial\CoreBundle\Form\ReceptionArticleType;

class ReceptionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('num')
            ->add('black', 'checkbox',[
                'label'=> 'sans facture',
                'required'=> false
            ])
            ->add('reception_articles', 'collection', array(
                'type' => new ReceptionArticleType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('warehouse', 'entity', array(
                'label'         =>'Dépot:',
                'empty_value'   =>'Choisissez un dépôt',
                'class'         =>'CommercialCoreBundle:Warehouse',
                'property'      =>'name'
            ))
            ->add('supplier', 'entity', array(
                'label'         =>'Fournisseur:',
                'empty_value'   =>'Choisissez un fournisseur',
                'class'         =>'CommercialCoreBundle:Supplier',
                'property'      =>'company_name'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Commercial\CoreBundle\Entity\Reception'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commercial_corebundle_reception';
    }
}
