<?php

namespace Commercial\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InvoiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('invoice_date', null, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'data' => new \DateTime()
	        ))
            ->add('num')
            ->add('discount')
            ->add('total')
            ->add('paid')
            ->add('payment_type')
            ->add('customer', 'entity', array(
                'label'         =>'Client:',
                'empty_value'   =>'Choisissez un client',
                'class'         =>'CommercialCoreBundle:Customer',
                'property'      =>'company_name'
            ))
            ->add('invoice_articles', 'collection', array(
                'type' => new InvoiceArticleType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('deadlines', 'collection', array(
                'type' => new DeadlineType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Commercial\CoreBundle\Entity\Invoice'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commercial_corebundle_invoice';
    }
}
