<?php

namespace Commercial\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityRepository;

class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('parent', 'entity', array(
                'label'         =>'Famille parent:',
                'empty_value'   =>'Choisissez une famille',
                'class'         =>'CommercialCoreBundle:Category',
                'query_builder' => function(EntityRepository $er) use($options) {
                                    return $er->findOthers($options['id']);
                                },
                'property'      => 'name',
                'required'      => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Commercial\CoreBundle\Entity\Category',
            'id'         => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commercial_corebundle_category';
    }
}
