<?php

namespace Commercial\CoreBundle\Event\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;

use Commercial\CoreBundle\Entity\Invoice;
use Commercial\CoreBundle\Entity\Quote;
use Commercial\CoreBundle\Entity\DeliveryOrder;

class PreListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Invoice) {
            $max = $em->getRepository('CommercialCoreBundle:Invoice')->findMaxNum();
            if(!empty($max)) {
                    $entity->setNum($max+1);
                } else {
                    $entity->setNum(1);
                }
        }
        
        if ($entity instanceof Quote) {
            $max = $em->getRepository('CommercialCoreBundle:Quote')->findMaxNum();
            if(!empty($max)) {
                    $entity->setNum($max+1);
                } else {
                    $entity->setNum(1);
                }
        }
        
        if ($entity instanceof DeliveryOrder) {
            $max = $em->getRepository('CommercialCoreBundle:DeliveryOrder')->findMaxNum();
            if(!empty($max)) {
                    $entity->setNum($max+1);
                } else {
                    $entity->setNum(1);
                }
        }
        }
}