<?php

namespace Commercial\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * WarehouseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class WarehouseRepository extends EntityRepository
{
    public function findList($itemPerPage,$page)
    {
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder()
                 ->select('w.id, w.name, w.address, w.description, w.tel, w.created_at')
                 ->from('CommercialCoreBundle:Warehouse', 'w')
                 ->orderBy('w.id', 'ASC')
                 ->setFirstResult(($page-1)*$itemPerPage)
                 ->setMaxResults($itemPerPage);
        
        return $qb->getQuery()->getResult();
    }
}
