<?php

namespace Commercial\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * StockEntryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class StockEntryRepository extends EntityRepository
{
}
