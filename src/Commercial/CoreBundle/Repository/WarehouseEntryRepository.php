<?php

namespace Commercial\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * WarehouseEntryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class WarehouseEntryRepository extends EntityRepository
{
    public function findList($itemPerPage,$page)
    {
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder()
                 ->select('we.id, we.created_at, we.quantity, we.amount, we.black, a.reference, a.buy_price, a.name, c.name as category')
                 ->from('CommercialCoreBundle:WarehouseEntry', 'we')
                 ->leftJoin('we.warehouse_article', 'wa')
                 ->leftJoin('wa.article', 'a')
                 ->leftJoin('a.category', 'c')
                 ->orderBy('we.created_at', 'DESC')
                 ->setFirstResult(($page-1)*$itemPerPage)
                 ->setMaxResults($itemPerPage);
        
        return $qb->getQuery()->getResult();
    }
}
