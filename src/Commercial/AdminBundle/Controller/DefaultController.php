<?php

namespace Commercial\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CommercialAdminBundle:Default:index.html.twig');
    }
}
